import type { UserResponse } from '@/models/userResponse'

export default defineNuxtRouteMiddleware(async (to: any) => {
  if (process.client && !to.path.startsWith('/api') && to.path !== '/login') {
    try {
      const data = sessionStorage.getItem('userdata')
      if (!data) {
        const response = (await $fetch('/api/auth/user', {
          headers: useRequestHeaders(['cookie']),
        })) as UserResponse
        sessionStorage.setItem('userdata', JSON.stringify(response.user))
      }
    } catch (exception: any) {
      if (exception.status === 403) {
        return navigateTo('/login')
      } else {
        /* eslint no-console: "off" */
        console.log(exception)
      }
    }
  }
})
