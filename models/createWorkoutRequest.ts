export interface CreateWorkoutRequest {
  date: string
  method: 'FromProgram' | 'CopyWorkout' | 'New'
  programId: string | null
  workoutId: string | null
}
