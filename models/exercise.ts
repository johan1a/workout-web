export const repsAndWeight = { value: 'repsAndWeight', text: 'Reps & Weight' }
export const reps = { value: 'reps', text: 'Reps' }
export const duration = { value: 'duration', text: 'Duration' }
export const distance = { value: 'distance', text: 'Distance' }

export interface Exercise {
  name: string
  exerciseType: string
  id: string | null
  userId: string
}
