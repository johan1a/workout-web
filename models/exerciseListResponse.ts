import type { Exercise } from '@/models/exercise'

export interface ExerciseListResponse {
  exercises: Array<Exercise>
}
