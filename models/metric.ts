export interface Metric {
  metricTypeId: string
  date: string
  value: number
  note: string | null
}
