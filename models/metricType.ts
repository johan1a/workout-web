export interface MetricType {
  id: string | null
  userId: string
  name: string
  minValue: number | null
  maxValue: number | null
}
