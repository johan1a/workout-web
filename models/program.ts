export interface TrainingMax {
  exerciseId: string
  weights: Array<number>
  defaultIncrement: number
}

export interface ProgramSet {
  index: number
  percentageOfMax: number | null
  fixedWeight: number | null
  nbrReps: number
}

export interface ProgramExercise {
  index: number
  exerciseId: string
  sets: Array<ProgramSet>
}

export interface ProgramDay {
  index: number
  exercises: Array<ProgramExercise>
}

export interface ProgramWeek {
  index: number
  days: Array<ProgramDay>
}

export interface ProgramData {
  weeks: Array<ProgramWeek>
  trainingMaxes: Array<TrainingMax>
  currentRound: number
  currentDay: number
  currentWeek: number
  offsetMode: number
  createdWorkoutIds: Array<string>
}

export interface Program {
  id: string | null
  programType: number
  name: string
  data: ProgramData
  userId: string
}
