import type { Program } from '@/models/program'

export interface ProgramListResponse {
  programs: Array<Program>
}
