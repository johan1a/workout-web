export interface RepMax {
  nbrReps: number
  weight: number
  date: string
  index: number
}

export interface ExerciseRecordData {
  exerciseId: string
  repMaxes: Array<RepMax>
}

export interface RecordData {
  userId: string
  exerciseRecords: Array<ExerciseRecordData>
}
