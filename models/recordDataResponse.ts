import type { RecordData } from '@/models/recordData'

export interface RecordDataResponse {
  records: RecordData
}
