export interface SelectOption {
  value: string | null
  text: string
}
