import type { User } from '@/models/user'

export interface UserResponse {
  user: User
}
