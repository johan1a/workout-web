export const RecordTypeReps = 0
export const RecordTypeDuration = 1

export interface RecordMarker {
  recordType: number
}

export interface ExerciseSet {
  index: number
  reps: number | null
  weight: number
  durationSeconds?: number | null
  checkedAt?: string | null
  recordMarkers?: RecordMarker
}

export interface WorkoutExercise {
  index: number
  exerciseId: string
  sets: Array<ExerciseSet>
}

export interface WorkoutData {
  exercises: Array<WorkoutExercise>
}

export interface Workout {
  id: string | null
  date: string
  data: WorkoutData
  programId: string | null
  userId: string
  version: number
}
