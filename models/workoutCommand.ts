import type { ExerciseSet, WorkoutExercise } from '@/models/workout'

export interface WorkoutCommand {
  commandType: string
  workoutId: string
}

export interface UpdateSetCommand extends WorkoutCommand {
  commandType: 'update-set'
  exerciseIndex: number
  exerciseSet: ExerciseSet
}

export interface CopySetCommand extends WorkoutCommand {
  commandType: 'copy-set'
  exerciseIndex: number
  setIndex: number
}

export interface DeleteSetCommand extends WorkoutCommand {
  commandType: 'delete-set'
  exerciseIndex: number
  setIndex: number
}

export interface ReorderSetsCommand extends WorkoutCommand {
  commandType: 'reorder-sets'
  exerciseIndex: number
  workoutId: string
  indices: number[]
}

export interface AddExerciseCommand extends WorkoutCommand {
  commandType: 'add-exercise'
  exerciseId: string
}

export interface DeleteExerciseCommand extends WorkoutCommand {
  commandType: 'delete-exercise'
  exercise: WorkoutExercise
}
