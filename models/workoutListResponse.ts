import type { Workout } from '@/models/workout'

export interface WorkoutListResponse {
  workouts: Array<Workout>
}
