import { defineNuxtConfig } from 'nuxt/config'

function getBackendUrl() {
  if (process.env.CI) {
    return 'http://workout-tracker:8080/api/**'
  } else if (process.env.NODE_ENV === 'development') {
    return `http://localhost:8080/api/**`
  } else {
    return 'https://workout.johanandersson.io/api/**'
  }
}

const backendUrl = getBackendUrl()

export default defineNuxtConfig({
  alias: {
    '~/*': './*',
    '@/*': './*',
    // Workaround from https://github.com/vitejs/vite/issues/13027#issuecomment-1527384805
    vite: 'node_modules/vite',
  },

  telemetry: false,

  app: {
    head: {
      title: 'Workout Tracker',
      htmlAttrs: {
        lang: 'en',
      },
      meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { hid: 'description', name: 'description', content: '' },
        { name: 'format-detection', content: 'telephone=no' },
      ],
      link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    },
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/scss/main.scss'],

  modules: ['@pinia/nuxt', '@nuxtjs/tailwindcss'],
  redirect: [{ from: '^/$', to: '/workouts' }],

  routeRules: {
    '/api/**': {
      proxy: backendUrl,
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    // Workaround for the following error:
    // Named export 'CategoryScale' not found. The requested module 'chart.js' is a CommonJS module, which may not support all module.exports as named exports.

    transpile: ['chart.js'],
  },

  buildModules: [],

  typescript: {
    typeCheck: {
      eslint: {
        files: './**/*.{ts,js,vue}',
      },
    },
  },

  vite: {
    optimizeDeps: {
      // Workaround for this dependency getting optimized in the middle of e2e tests, the page reloading and the test failing.
      exclude: ['vue-draggable-next'],
    },
  },

  compatibilityDate: '2024-10-20',
})
