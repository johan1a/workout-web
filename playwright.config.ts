import { defineConfig, devices } from '@playwright/test'

/**
 * Read environment variables from file.
 * https://github.com/motdotla/dotenv
 */
// import dotenv from 'dotenv';
// import path from 'path';
// dotenv.config({ path: path.resolve(__dirname, '.env') });

const reporters: any = process.env.CI
  ? [
      ['dot', {}],
      ['html', {}],
      ['./test/e2e/upload-html-reporter.ts', {}],
    ]
  : [['html', {}]]

/**
 * See https://playwright.dev/docs/test-configuration.
 */
export default defineConfig({
  testDir: './test/e2e',
  /* Run tests in files in parallel */
  fullyParallel: true,
  /* Fail the build on CI if you accidentally left test.only in the source code. */
  forbidOnly: !!process.env.CI,

  retries: process.env.CI ? 0 : 0,

  workers: process.env.CI ? 3 : undefined,
  /* Reporter to use. See https://playwright.dev/docs/test-reporters */
  reporter: reporters,
  /* Shared settings for all the projects below. See https://playwright.dev/docs/api/class-testoptions. */
  use: {
    /* Base URL to use in actions like `await page.goto('/')`. */
    baseURL: 'http://localhost:3000',

    /* Collect trace when retrying the failed test. See https://playwright.dev/docs/trace-viewer */
    trace: 'on-first-retry',

    headless: true,

    screenshot: 'only-on-failure',
  },

  timeout: process.env.CI ? 30_000 : 5_000,

  expect: {
    timeout: process.env.CI ? 10_000 : 3_000,
  },

  /* Configure projects for major browsers */
  projects: [
    { name: 'login', testMatch: /e2e.login.ts/ },
    {
      name: 'setup',
      testMatch: /.*\.setup\.ts/,
      use: {
        storageState: 'test/e2e/.auth/user.json',
      },
      dependencies: ['login'],
    },
    // {
    //   name: 'chromium',
    //   use: {
    //     ...devices['Desktop Chrome'],
    //     storageState: 'test/e2e/.auth/user.json',
    //   },
    //   dependencies: ['setup'],
    // },

    {
      name: 'firefox',
      use: {
        ...devices['Desktop Firefox'],
        storageState: 'test/e2e/.auth/user.json',
      },
      dependencies: ['setup'],
    },
    // https://github.com/microsoft/playwright/blob/main/packages/playwright-core/src/server/deviceDescriptorsSource.json
    {
      name: 'Mobile Chrome',
      use: {
        ...devices['Pixel 7'],
        storageState: 'test/e2e/.auth/user.json',
      },
      dependencies: ['setup'],
    },
  ],

  /* Run your local dev server before starting the tests */
  webServer: {
    command: 'yarn dev',
    url: 'http://localhost:3000',
    reuseExistingServer: !process.env.CI,
    stdout: 'pipe',
    stderr: 'pipe',
  },
})
