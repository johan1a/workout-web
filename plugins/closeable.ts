export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.directive('click-outside', {
    beforeMount(element, binding) {
      element.clickOutsideEvent = function (event: any) {
        if (!(element === event.target || element.contains(event.target))) {
          binding.value(event)
        }
      }
      document.body.removeEventListener('click', element.clickOutsideEvent)
      document.body.addEventListener('click', element.clickOutsideEvent)
    },
  })
})
