#!/bin/sh
GIT_SHA=$(git rev-parse HEAD)
docker pull johan1a/workout-web:latest || true
docker build --cache-from johan1a/workout-web:latest -t johan1a/workout-web:latest -t johan1a/workout-web:${GIT_SHA} .
