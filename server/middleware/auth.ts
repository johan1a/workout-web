export default defineEventHandler((event) => {
  const url = event.node.req.url
  if (url && !url.startsWith('/api') && url !== '/login') {
    const cookies = parseCookies(event)
    const sessiondata = cookies._sessiondata
    const refreshToken = cookies._refreshtoken
    if (!sessiondata && !refreshToken) {
      return sendRedirect(event, '/login')
    }
  }
})
