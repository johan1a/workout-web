export function focusOnInput(event: Event) {
  if (event.target instanceof HTMLInputElement) {
    event.target?.select()
  }
}
