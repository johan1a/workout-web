import type { Workout, ExerciseSet } from '~/models/workout'

export const maxWeight = 'maxWeight'
export const maxSetVolume = 'maxSetVolume'
export const maxReps = 'maxReps'
export const maxDuration = 'maxDuration'
export const totalReps = 'totalReps'
export const totalVolume = 'totalVolume'
export const totalDuration = 'totalDuration'

class StatisticsService {
  getMaxes(workouts: Array<Workout>, type: string, selectedExerciseId: string): [number[], string[], string[]] {
    const maxes: number[] = []
    const labels = []
    const dates = []

    let compareFunc = (a: ExerciseSet, b: ExerciseSet) => a.weight > b.weight
    if (type === maxReps) {
      compareFunc = (a: ExerciseSet, b: ExerciseSet) => a.reps === null || b.reps == null || a.reps > b.reps
    } else if (type === maxDuration) {
      compareFunc = (a: ExerciseSet, b: ExerciseSet) =>
        a.durationSeconds == null || b.durationSeconds == null || a.durationSeconds > b.durationSeconds
    } else if (type === maxSetVolume) {
      compareFunc = (a: ExerciseSet, b: ExerciseSet) =>
        a.reps === null || b.reps == null || this.getSetVolume(a) > this.getSetVolume(b)
    }

    for (const workout of workouts) {
      for (const exercise of workout.data.exercises) {
        if (exercise.exerciseId === selectedExerciseId) {
          let best: ExerciseSet | undefined
          for (const set of exercise.sets) {
            if (set.checkedAt && (best === undefined || compareFunc(set, best))) {
              best = set
            }
          }
          if (best) {
            if (type === maxWeight) {
              maxes.push(best.weight)
            } else if (type === maxReps) {
              maxes.push(best.reps !== null ? best.reps : 0)
            } else if (type === maxDuration) {
              maxes.push(best.durationSeconds != null ? best.durationSeconds : 0)
            } else if (type === maxSetVolume) {
              maxes.push(this.getSetVolume(best))
              labels.push(best.reps + ' x ' + best.weight)
            }
            dates.push(workout.date)
          }
        }
      }
    }
    return [maxes, labels, dates]
  }

  getTotal(workouts: Array<Workout>, type: string, selectedExerciseId: string): [number[], string[]] {
    const totals = []
    const dates = []

    for (const workout of workouts) {
      let total = 0
      for (const exercise of workout.data.exercises) {
        if (exercise.exerciseId === selectedExerciseId) {
          for (const set of exercise.sets) {
            if (set.checkedAt) {
              if (type === totalVolume) {
                total += set.weight * (set.reps != null ? set.reps : 0)
              } else if (type === totalReps) {
                total += set.reps !== null ? set.reps : 0
              } else if (type === totalDuration) {
                total += set.durationSeconds ? set.durationSeconds : 0
              }
            }
          }
        }
      }
      if (total > 0) {
        totals.push(total)
        dates.push(workout.date)
      }
    }
    return [totals, dates]
  }

  getAverage(numbers: Array<number>): number[] {
    const average = []
    const N = 8

    // Define weights
    const weights = []
    for (let i = 1; i <= N; i++) {
      weights.push(i)
    }

    const weightSum = weights.reduce((a, b) => a + b, 0)

    for (let i = 0; i < numbers.length; i++) {
      if (i === 0) {
        average.push(numbers[i])
      } else if (i < N) {
        let sum = 0
        for (let j = 0; j < i; j++) {
          sum += numbers[i - j]
        }
        average.push(sum / i)
      } else {
        let sum = 0
        const end = i < N ? i : N
        for (let j = 0; j < end; j++) {
          sum += numbers[i - j] * weights[N - 1 - j]
        }
        average.push(sum / weightSum)
      }
    }

    return average
  }

  private getSetVolume(set: ExerciseSet): number {
    return set.weight * (set.reps ? set.reps : 0)
  }
}

export const statisticsService = new StatisticsService()
