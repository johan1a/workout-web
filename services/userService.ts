import type { User } from '~/models/user'

class UserService {
  fetchUser(): User | null {
    if (process.server) {
      return null
    }
    const data = sessionStorage.getItem('userdata')
    if (data) {
      return JSON.parse(data)
    } else {
      return null
    }
  }

  async logout() {
    try {
      await $fetch('/api/auth/logout', {
        method: 'POST',
        headers: useRequestHeaders(['cookie']),
      })
    } catch (e: any) {
      /* eslint no-console: "off" */
      console.log('Got error when logging out:', e)
    }
    sessionStorage.removeItem('userdata')
    navigateTo('/login')
  }
}

export const userService = new UserService()
