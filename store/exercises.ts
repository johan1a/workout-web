import { defineStore } from 'pinia'
import type { Exercise } from '~/models/exercise'
import type { ExerciseListResponse } from '~/models/exerciseListResponse'
import { fetchWithRetry } from '~/utils/retryUtils'

export const useExercisesStore = defineStore('exercises', {
  state: () => {
    return {
      exercises: [] as Array<Exercise>,
    }
  },
  getters: {
    list(state): Exercise[] {
      return state.exercises
    },
  },
  actions: {
    async fetchExercises() {
      try {
        const response = (await fetchWithRetry('/api/exercises', {
          headers: useRequestHeaders(['cookie']),
        })) as ExerciseListResponse

        this.exercises = response.exercises
        return response.exercises
      } catch (error: any) {
        if (error && String(error.message).includes('403')) {
          return await navigateTo('/login')
        }

        return error
      }
    },
    findById(id: string): Exercise | null {
      const found = this.exercises.find((e: Exercise) => e.id === id)
      if (found !== undefined) {
        return found
      } else {
        return null
      }
    },
    exerciseName(id: string): string {
      const exercise = this.findById(id)
      return exercise?.name !== undefined ? exercise.name : ''
    },
    async save(exercise: Exercise) {
      const response = await fetchWithRetry('/api/exercises', {
        method: 'POST',
        headers: useRequestHeaders(['cookie']),
        body: JSON.stringify({
          id: exercise.id,
          name: exercise.name,
          userId: exercise.userId,
          exerciseType: exercise.exerciseType,
        }),
      })

      const savedExercise = response as Exercise
      this.exercises = this.exercises.filter((e: Exercise) => e.id !== exercise.id)
      this.exercises.push(savedExercise)
      return savedExercise
    },
  },
})
