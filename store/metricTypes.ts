import { defineStore } from 'pinia'
import type { MetricType } from '~/models/metricType'
import { fetchWithRetry } from '~/utils/retryUtils'

export const useMetricTypesStore = defineStore('metricTypes', {
  state: () => {
    return {
      metricTypes: [] as MetricType[],
    }
  },
  getters: {
    list(state): MetricType[] {
      return state.metricTypes
    },
  },
  actions: {
    async fetchMetricTypes() {
      try {
        const response = await fetchWithRetry('/api/metric-types', {
          headers: useRequestHeaders(['cookie']),
        })

        this.metricTypes = response as Array<MetricType>
        return response
      } catch (error: any) {
        if (error.message.startsWith('403')) {
          return await navigateTo('/login')
        }

        return error
      }
    },
    findById(id: string): MetricType | null {
      const found = this.metricTypes?.find((e: MetricType) => e.id === id)
      if (found !== undefined) {
        return found
      } else {
        return null
      }
    },
    async save(metricType: MetricType) {
      const response = await fetchWithRetry('/api/metric-types', {
        method: 'POST',
        headers: useRequestHeaders(['cookie']),
        body: JSON.stringify(metricType),
      })

      const savedMetricType: MetricType = response as MetricType
      this.metricTypes = this.metricTypes.filter((e: MetricType) => e.id !== metricType.id)
      this.metricTypes.push(savedMetricType)
      return savedMetricType
    },
  },
})
