import { defineStore } from 'pinia'
import type { Metric } from '~/models/metric'
import { fetchWithRetry } from '~/utils/retryUtils'

export const useMetricsStore = defineStore('metrics', {
  state: () => {
    return {
      metrics: new Map<string, Array<Metric>>(),
    }
  },
  getters: {
    getMetrics(state): Map<string, Array<Metric>> {
      return state.metrics
    },
  },
  actions: {
    async fetchMetrics(metricTypeId: string): Promise<Array<Metric>> {
      try {
        const response = await fetchWithRetry('/api/metric-types/' + metricTypeId + '/metrics', {
          headers: useRequestHeaders(['cookie']),
        })

        const metrics = response as Metric[]
        this.metrics.set(metricTypeId, metrics)
        return metrics
      } catch (error: any) {
        if (error.message.startsWith('403')) {
          await navigateTo('/login')
        }

        return error
      }
    },
    async save(metric: Metric) {
      const response = await fetchWithRetry('/api/metric-types/' + metric.metricTypeId + '/metrics', {
        method: 'POST',
        headers: useRequestHeaders(['cookie']),
        body: JSON.stringify(metric),
      })

      const savedMetric: Metric = response as Metric
      const metrics = this.metrics.get(metric.metricTypeId) ?? []
      metrics.push(savedMetric)
      this.metrics.set(metric.metricTypeId, metrics)
      return savedMetric
    },
  },
})
