import { defineStore } from 'pinia'
import type { Program } from '~/models/program'
import type { ProgramListResponse } from '~/models/programListResponse'

export const OFFSET_MODE_1 = 1

export const useProgramsStore = defineStore('programs', {
  state: () => {
    return {
      programs: [] as Array<Program>,
    }
  },
  actions: {
    async fetchPrograms(): Promise<void> {
      if (this.programs.length === 0) {
        await this.doFetchPrograms()
      }
    },

    async doFetchPrograms(): Promise<void> {
      const response = (await $fetch('/api/programs', {
        headers: useRequestHeaders(['cookie']),
      })) as ProgramListResponse

      if (response) {
        this.programs = response.programs
      }
    },

    async save(program: Program): Promise<Program> {
      const savedProgram = (await $fetch('/api/programs', {
        method: 'POST',
        headers: useRequestHeaders(['cookie']),
        body: JSON.stringify(program),
      })) as Program

      const updatedProgramList = this.programs.map(function (existing: Program) {
        if (existing.id === savedProgram.id) {
          return savedProgram
        } else {
          return existing
        }
      })

      if (!updatedProgramList.find((w) => w.id === savedProgram.id)) {
        updatedProgramList.push(savedProgram)
      }
      this.programs = updatedProgramList
      return savedProgram
    },

    async delete(program: Program) {
      await $fetch('/api/programs/' + program.id, {
        method: 'DELETE',
        headers: useRequestHeaders(['cookie']),
      })

      this.programs = this.programs.filter(function (existing: Program) {
        return existing.id !== program.id
      })
    },

    round(n: number): number {
      return Math.ceil(n / 2.5) * 2.5
    },
  },
})
