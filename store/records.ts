import { defineStore } from 'pinia'
import type { RecordData } from '@/models/recordData'
import type { RecordDataResponse } from '@/models/recordDataResponse'
import type { ExerciseSet, WorkoutExercise } from '@/models/workout'

export const useRecordsStore = defineStore('records', {
  state: () => {
    return {
      records: [] as Array<RecordData>,
    }
  },
  actions: {
    async fetchRecords() {
      try {
        const response = (await $fetch('/api/records', {
          headers: useRequestHeaders(['cookie']),
        })) as RecordDataResponse

        this.records = [response.records]
      } catch (error) {
        return error
      }
    },
    isRecord(exerciseId: string, workoutDate: string, exerciseSet: ExerciseSet): boolean {
      if (this.records.length === 0) {
        return false
      } else {
        const recordsForExercise = this.records[0].exerciseRecords.find((r) => r.exerciseId === exerciseId)
        if (!recordsForExercise || exerciseSet.checkedAt === undefined) {
          return false
        } else {
          const record = recordsForExercise.repMaxes.find(
            (r) =>
              r.nbrReps === exerciseSet.reps &&
              r.weight === exerciseSet.weight &&
              r.index === exerciseSet.index &&
              r.date === workoutDate,
          )
          return record !== undefined
        }
      }
    },
    getExerciseRecordText(exercise: WorkoutExercise, date: string): string {
      const records = new Set(
        exercise.sets
          .map((set) => this.getSetRecordTextAndEmoji(exercise.exerciseId, date, set))
          .filter((s) => s !== ''),
      )
      return [...records].join(', ')
    },
    getSetRecordText(exerciseId: string, workoutDate: string, exerciseSet: ExerciseSet): string {
      if (this.isRecord(exerciseId, workoutDate, exerciseSet)) {
        return exerciseSet.reps + 'RM'
      } else {
        return ''
      }
    },
    getSetRecordEmoji(exerciseId: string, workoutDate: string, exerciseSet: ExerciseSet): string {
      if (this.isRecord(exerciseId, workoutDate, exerciseSet)) {
        const emojis = ['🏆', '🏅', '🎖️', '🎉', '✨', '🌟', '🌠', '💪', '🍾']
        const index = Math.floor(exerciseSet.weight) % emojis.length
        return emojis[index]
      } else {
        return ''
      }
    },
    getSetRecordTextAndEmoji(exerciseId: string, workoutDate: string, exerciseSet: ExerciseSet): string {
      if (this.isRecord(exerciseId, workoutDate, exerciseSet)) {
        const emoji = this.getSetRecordEmoji(exerciseId, workoutDate, exerciseSet)
        return emoji + ' ' + exerciseSet.reps + 'RM'
      } else {
        return ''
      }
    },
  },
})
