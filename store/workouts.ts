import { defineStore } from 'pinia'
import type { ExerciseSet, Workout, WorkoutExercise } from '~/models/workout'
import type { CreateWorkoutRequest } from '~/models/createWorkoutRequest'
import type { WorkoutListResponse } from '~/models/workoutListResponse'
import { clone } from '~/utils/clone'
import { fetchWithRetry } from '~/utils/retryUtils'
import {
  type WorkoutCommand,
  type UpdateSetCommand,
  type AddExerciseCommand,
  type ReorderSetsCommand,
  type CopySetCommand,
  type DeleteSetCommand,
  type DeleteExerciseCommand,
} from '~/models/workoutCommand'

export const useWorkoutsStore = defineStore('workouts', {
  state: () => {
    return {
      workouts: [] as Array<Workout>,
      commands: [] as Array<WorkoutCommand>,
      isProcessingCommands: false,
    }
  },

  actions: {
    async fetchWorkouts(size: number) {
      try {
        const response = (await fetchWithRetry('/api/workouts?size=' + size, {
          headers: useRequestHeaders(['cookie']),
        })) as WorkoutListResponse

        this.workouts = response.workouts
      } catch (error) {
        return error
      }
    },

    async fetchById(id: string): Promise<Workout> {
      const response = await fetchWithRetry(`/api/workouts/${id}/`, {
        headers: useRequestHeaders(['cookie']),
      })
      const workout = response as Workout

      if (workout) {
        this.workouts.push(workout)
      }
      return workout
    },

    async fetch(offset: number, size: number): Promise<Workout[]> {
      const response = (await fetchWithRetry('/api/workouts?size=' + size + '&offset=' + offset, {
        headers: useRequestHeaders(['cookie']),
      })) as WorkoutListResponse
      const foundWorkouts = response.workouts

      const unique = [...new Set(this.workouts.concat(sortWorkouts(foundWorkouts)))]
      this.workouts = unique
      return foundWorkouts
    },

    async find(id: string): Promise<Workout | undefined> {
      const workout = this.workouts.find((w) => w.id === id)
      if (!workout) {
        return await this.fetchById(id)
      }
      return workout
    },

    async processCommand(newCommand: WorkoutCommand): Promise<boolean> {
      this.commands.push(newCommand)

      if (!this.isProcessingCommands) {
        this.isProcessingCommands = true
        while (this.commands.length > 0) {
          const command: WorkoutCommand | undefined = this.commands.shift()
          if (command) {
            const workout = await this.find(command.workoutId)
            if (workout) {
              /* eslint no-console: "off" */
              console.log('processing command', JSON.stringify(command))
              const updatedWorkout = doMutations(workout, command)
              try {
                await this.save(updatedWorkout)
              } catch (e: any) {
                this.isProcessingCommands = false
                throw e
              }
            } else {
              this.isProcessingCommands = false
              throw new Error('workout not found')
            }
          }
        }
        this.isProcessingCommands = false
        return true
      }
      return false
    },

    async save(workout: Workout): Promise<Workout> {
      const workoutWithIncrementedVersion = clone(workout)
      if (workout.id) {
        workoutWithIncrementedVersion.version = workout.version + 1
      }

      const response = await fetchWithRetry('/api/workouts', {
        method: 'POST',
        headers: useRequestHeaders(['cookie']),
        body: JSON.stringify(workoutWithIncrementedVersion),
      })
      const savedWorkout = response as Workout

      this.updateWorkoutList(savedWorkout)

      return savedWorkout
    },

    async createWorkout(createWorkoutRequest: CreateWorkoutRequest): Promise<Workout> {
      const response = await fetchWithRetry('/api/workouts/create', {
        method: 'POST',
        headers: useRequestHeaders(['cookie']),
        body: JSON.stringify(createWorkoutRequest),
      })
      const savedWorkout = response as Workout

      this.updateWorkoutList(savedWorkout)

      return savedWorkout
    },

    async createNew(date: string): Promise<Workout | undefined> {
      const request: CreateWorkoutRequest = {
        date: date,
        method: 'New',
        workoutId: null,
        programId: null,
      }
      return await this.createWorkout(request)
    },

    async copy(date: string, workoutId: string): Promise<Workout | undefined> {
      const request: CreateWorkoutRequest = {
        date: date,
        method: 'CopyWorkout',
        workoutId: workoutId,
        programId: null,
      }
      return await this.createWorkout(request)
    },

    async createFromProgram(date: string, programId: string): Promise<Workout | undefined> {
      const request: CreateWorkoutRequest = {
        date: date,
        method: 'FromProgram',
        workoutId: null,
        programId: programId,
      }
      return await this.createWorkout(request)
    },

    async delete(workout: Workout): Promise<void> {
      const url = '/api/workouts/' + workout.id
      const statusCode = (await fetch(url, { method: 'DELETE' })).status
      if (statusCode === 200) {
        const index = this.workouts.indexOf(workout)
        this.workouts.splice(index, 1)
      }
    },

    updateWorkoutList(newWorkout: Workout) {
      const updatedWorkoutList = this.workouts.map(function (existing: Workout) {
        if (existing.id === newWorkout.id) {
          return newWorkout
        } else {
          return existing
        }
      })

      if (!updatedWorkoutList.find((w) => w.id === newWorkout.id)) {
        updatedWorkoutList.push(newWorkout)
      }
      this.workouts = sortWorkouts(updatedWorkoutList)
    },
  },
})

function doMutations(workout: Workout, command: WorkoutCommand): Workout {
  if (command.commandType === 'update-set') {
    return updateSetInWorkout(workout, command as UpdateSetCommand)
  } else if (command.commandType === 'delete-set') {
    return deleteSet(workout, command as DeleteSetCommand)
  } else if (command.commandType === 'copy-set') {
    return copySet(workout, command as CopySetCommand)
  } else if (command.commandType === 'reorder-sets') {
    return reorderSets(workout, command as ReorderSetsCommand)
  } else if (command.commandType === 'add-exercise') {
    return addExerciseToWorkout(workout, command as AddExerciseCommand)
  } else if (command.commandType === 'delete-exercise') {
    return deleteExercise(workout, command as DeleteExerciseCommand)
  } else {
    throw new Error('unknown command type: ' + JSON.stringify(command))
  }
}

function updateSetInWorkout(workout: Workout, command: UpdateSetCommand) {
  const exerciseIndex = command.exerciseIndex
  const exerciseSet = command.exerciseSet
  const workoutExercise = workout.data.exercises[exerciseIndex]
  const updatedSet: ExerciseSet = clone(exerciseSet)

  const updatedExercise = clone(workoutExercise)
  updatedExercise.sets.push(updatedSet)

  const updatedWorkout: Workout = clone(workout)
  updatedWorkout.data.exercises = updatedWorkout.data.exercises.map(function (existing: WorkoutExercise) {
    if (existing.index === workoutExercise.index) {
      if (existing.sets.find((e) => e.index === updatedSet.index) !== undefined) {
        existing.sets = existing.sets.map((e) => updateSet(e, updatedSet))
      } else {
        existing.sets.push(updatedSet)
      }
    }
    return existing
  })

  return updatedWorkout
}

function addExerciseToWorkout(workout: Workout, command: AddExerciseCommand): Workout {
  const exerciseId = command.exerciseId
  const exercise: WorkoutExercise = {
    exerciseId: exerciseId,
    sets: [
      {
        index: 0,
        reps: 0,
        weight: 0,
        durationSeconds: null,
        checkedAt: null,
      },
    ],
    index: workout.data.exercises.length,
  }
  const updatedWorkout: Workout = clone(workout)
  updatedWorkout.data.exercises.push(exercise)
  return updatedWorkout
}

function deleteExercise(workout: Workout, command: DeleteExerciseCommand): Workout {
  const index = workout.data.exercises.indexOf(command.exercise)
  if (index && index !== -1) {
    const updatedWorkout = clone(workout)
    updatedWorkout.data.exercises.splice(index, 1)
    for (let i: number = 0; i < updatedWorkout.data.exercises.length; i++) {
      updatedWorkout.data.exercises[i].index = i
    }
    return updatedWorkout
  } else {
    throw new Error(
      'Cannot delete exercise, command: ' + JSON.stringify(command) + ' workout: ' + JSON.stringify(workout),
    )
  }
}

function updateSet(existingSet: ExerciseSet, updatedSet: ExerciseSet): ExerciseSet {
  if (existingSet.index === updatedSet.index) {
    return updatedSet
  } else {
    return existingSet
  }
}

function copySet(workout: Workout, command: CopySetCommand): Workout {
  const workoutExercise = workout.data.exercises[command.exerciseIndex]
  const exerciseSet: ExerciseSet = clone(workoutExercise.sets[command.setIndex])
  const setCopy: ExerciseSet = clone(exerciseSet)
  setCopy.checkedAt = null

  const updatedExercise = clone(workoutExercise)

  updatedExercise.sets.splice(command.setIndex + 1, 0, setCopy)

  for (let i = 0; i < updatedExercise.sets.length; i++) {
    updatedExercise.sets[i].index = i
  }

  const updatedWorkout: Workout = clone(workout)
  updatedWorkout.data.exercises[command.exerciseIndex] = updatedExercise

  return updatedWorkout
}

function deleteSet(workout: Workout, command: DeleteSetCommand): Workout {
  const workoutExercise = workout.data.exercises[command.exerciseIndex]
  const exerciseSet: ExerciseSet = clone(workoutExercise.sets[command.setIndex])
  const setToDelete: ExerciseSet = clone(exerciseSet)

  const updatedExercise = clone(workoutExercise)
  updatedExercise.sets.push(setToDelete)

  const updatedWorkout: Workout = clone(workout)
  updatedWorkout.data.exercises = updatedWorkout.data.exercises.map(function (existing: WorkoutExercise) {
    if (existing.index === workoutExercise.index) {
      const foundSet = existing.sets.find((s) => s.index === setToDelete.index)
      if (foundSet !== undefined) {
        existing.sets.splice(foundSet.index, 1)
        for (let i: number = 0; i < existing.sets.length; i++) {
          existing.sets[i].index = i
        }
      }
    }
    return existing
  })

  return updatedWorkout
}

function reorderSets(workout: Workout, command: ReorderSetsCommand): Workout {
  const workoutCopy = clone(workout)
  const sets = workoutCopy.data.exercises[command.exerciseIndex].sets
  sets.forEach((set, index) => {
    set.index = command.indices.indexOf(index)
  })
  const sortedSets = sets.sort((s1, s2) => (s1.index < s2.index ? -1 : 1))
  workoutCopy.data.exercises[command.exerciseIndex].sets = sortedSets
  return workoutCopy
}

function sortWorkouts(workouts: Array<Workout>): Array<Workout> {
  const sortedList = workouts.sort((n1, n2) => {
    if (n1 === n2) {
      return 0
    }
    return n1.date < n2.date ? 1 : -1
  })
  return sortedList
}
