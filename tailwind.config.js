/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [],
  theme: {
    extend: {
      colors: {
        base: '#2E215B',
        'base-1': '#4b215b',
        'base-2': '#21315b',
      },
    },
  },
  plugins: [],
}
