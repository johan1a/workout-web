export function attachConsole(page: any) {
  page.on('console', (msg: any) => {
    /* eslint no-console: "off" */
    console.log('Console message:', msg.text())
    /* eslint no-console: "off" */
    console.log('Type:', msg.type())
    /* eslint no-console: "off" */
    console.log('Location:', msg.location())
    if (msg.type() === 'error') {
      /* eslint no-console: "off" */
      console.log('Error Text:', msg.text())
    }
  })
}
