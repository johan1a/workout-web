import path from 'path'
import { test as setup, expect } from '@playwright/test'

const authFile = path.join(__dirname, '.auth/user.json')

setup('authenticate', async ({ page }) => {
  const testId = String(new Date().getTime())
  if (!process.env.TEST_ID) {
    process.env.TEST_ID = testId
  }
  /* eslint no-console: "off" */
  console.log('testId', testId)

  await page.goto('/login')

  await expect(page.getByRole('heading', { name: 'Login' })).toBeVisible()

  await page.getByPlaceholder('Username').fill('testuser')

  await page.getByPlaceholder('Password').fill('testpassword')

  await page.getByRole('button', { name: 'Sign In' }).click()
  await page.waitForLoadState('networkidle')

  await expect(page.getByRole('heading', { name: 'Workouts' })).toBeVisible()

  await page.context().storageState({ path: authFile })
})
