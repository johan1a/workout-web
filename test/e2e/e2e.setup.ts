import { test as setup, expect } from '@playwright/test'

setup('Create exercises', async ({ page }) => {
  const testId = process.env.TEST_ID

  const deadlift = 'Deadlift-' + testId
  const benchPress = 'Bench press-' + testId
  await page.goto('/exercises')

  await page.locator('#link-create-exercise').click()
  await page.waitForLoadState('networkidle')

  await expect(page.locator('.exercise-header')).toHaveText('Create new exercise')
  await page.locator('#input-name').fill(deadlift)
  await expect(page.locator('#input-name')).toHaveValue(deadlift)

  await page.locator('#button-submit').click()
  await page.waitForLoadState('networkidle')

  await expect(page.getByRole('heading', { name: 'Exercises' })).toBeVisible()
  await expect(page.getByText(deadlift)).toBeVisible()

  await page.locator('#link-create-exercise').click()
  await page.waitForLoadState('networkidle')

  await expect(page.locator('.exercise-header')).toHaveText('Create new exercise')
  await page.locator('#input-name').fill(benchPress)
  await expect(page.locator('#input-name')).toHaveValue(benchPress)

  await page.locator('#button-submit').click()
  await page.waitForLoadState('networkidle')

  await expect(page.getByText(deadlift)).toBeVisible()
  await expect(page.getByRole('heading', { name: 'Exercises' })).toBeVisible()
  await expect(page.getByText(benchPress)).toBeVisible()
})

setup('Create a metric type', async ({ page }) => {
  const testId = process.env.TEST_ID

  await page.goto('/metric-types')
  await page.waitForLoadState('networkidle')

  await expect(page.getByRole('heading', { name: 'Metric Types' })).toBeVisible()

  await page
    .getByPlaceholder('Enter name')
    .nth(0)
    .fill('Some metric-' + testId)
  await page.waitForLoadState('networkidle')

  await page.getByRole('button', { name: 'Submit' }).nth(0).click()
})
