import { test, expect } from '@playwright/test'

test('Register metric type', async ({ page }) => {
  const testId = process.env.TEST_ID
  const metricName = 'Weight-' + testId

  await page.goto('/metrics')
  await page.waitForLoadState('networkidle')

  await page.getByRole('link', { name: 'Edit metric types' }).click()

  await expect(page.getByRole('heading', { name: 'Metric Types' })).toBeVisible()

  await page.getByPlaceholder('Enter name').nth(0).fill(metricName)

  await page.getByRole('button', { name: 'Submit' }).nth(0).click()
  await page.waitForLoadState('networkidle')

  await page.goto('/metrics')

  await expect(page.getByRole('heading', { name: 'Metrics' })).toBeVisible()
})

test('Register metric', async ({ page }) => {
  const testId = process.env.TEST_ID
  const metricName = 'Some metric-' + testId

  await page.goto('/metrics')
  await page.waitForLoadState('networkidle')

  await expect(page.getByRole('heading', { name: 'Metrics' })).toBeVisible()

  await page.locator('#selectMetricTypeId').selectOption(metricName)

  await page.getByRole('link', { name: 'Register data' }).click()
  await page.waitForLoadState('networkidle')

  await expect(page.getByRole('heading', { name: 'Register data' })).toBeVisible()
  await expect(page.locator('.metric-header')).toHaveText(metricName)

  await page.getByPlaceholder('Enter value').fill('123')

  await page.getByRole('button', { name: 'Submit' }).click()

  await expect(page.getByRole('heading', { name: 'Metrics' })).toBeVisible()
})
