import { test, expect } from '@playwright/test'

test('Show programs page', async ({ page }) => {
  await page.goto('/programs')

  await expect(page.getByRole('heading', { name: 'Programs' })).toBeVisible()
})

test.describe.serial('Running test sequentially', () => {
  const testId = process.env.TEST_ID
  const programName = 'e2e program-' + testId + '-' + String(new Date().getTime())
  const benchPress = 'Bench press-' + testId

  test('Create new program', async ({ page }) => {
    await page.goto('/programs')
    await page.waitForLoadState('networkidle')

    await page.locator('#link-create-program').click()
    await page.waitForLoadState('networkidle')

    await expect(page.getByRole('heading', { name: 'Edit program' })).toBeVisible()

    await page.getByPlaceholder('Enter name').fill(programName)

    await page.locator('.add-training-max').click()
    await page.locator('.add-training-max').click()
    await page.locator('.remove-training-max').click()

    await page.locator('.select-exercise').selectOption(benchPress)

    await page.locator('.input-training-max').fill('100')
    await page.locator('.input-increase-per-round').fill('2.5')

    await page.getByRole('button', { name: 'Add exercise' }).click()

    await page.locator('.select-program-exercise').selectOption(benchPress)

    await page.locator('.button-add-set').click()
    await page.locator('.input-set-percentage-of-max').fill('80')
    await page.locator('.input-set-nbr-reps').fill('5')

    await page.locator('.button-add-set').click()
    await page.locator('.button-remove-set').nth(1).click()
    await page.locator('.button-add-set').click()

    await page.locator('.select-program-set-type').nth(1).selectOption('kg')
    await page.locator('.input-set-fixed-weight').fill('50')
    await page.locator('.input-set-nbr-reps').nth(1).fill('8')

    await page.getByRole('button', { name: 'Add week' }).click()
    await page.getByRole('button', { name: 'Add day' }).nth(1).click()
    await page.getByRole('button', { name: 'Add exercise' }).nth(1).click()

    await page.locator('.select-program-exercise').nth(1).selectOption(benchPress)
    await page.locator('.button-add-set').nth(1).click()
    await page.locator('.input-set-percentage-of-max').nth(1).fill('90')
    await page.locator('.input-set-nbr-reps').nth(2).fill('10')

    await page.getByRole('button', { name: 'Save program' }).click()
    await page.waitForLoadState('networkidle')

    await expect(page.getByRole('heading', { name: programName })).toBeVisible()
  })

  test('Create workout from program', async ({ page }) => {
    await page.goto('/workouts/create')
    await page.waitForLoadState('networkidle')

    await page.locator('#select-program').selectOption(programName)

    await page.getByRole('button', { name: 'Create' }).click()
    await page.waitForLoadState('networkidle')

    await expect(page.getByRole('heading', { name: benchPress })).toBeVisible()

    await expect(page.locator('.set-weight').nth(0)).toHaveValue('80')
    await expect(page.locator('.set-reps').nth(0)).toHaveValue('5')

    await expect(page.locator('.set-weight').nth(1)).toHaveValue('50')
    await expect(page.locator('.set-reps').nth(1)).toHaveValue('8')

    await page.reload()

    await expect(page.getByRole('heading', { name: benchPress })).toBeVisible()
  })
})
