/* eslint-disable no-console */
import fs from 'fs'
import path from 'path'
import type { FullConfig, Reporter, Suite } from '@playwright/test/reporter'
import { $fetch } from 'ofetch'

const uploadUrl = process.env.REPORT_UPLOAD_URL || 'http://localhost:8000'
const uploadUsername = process.env.REPORT_UPLOAD_USERNAME
const uploadPassword = process.env.REPORT_UPLOAD_PASSWORD

const now: Date = new Date()
const reportBaseDir = 'playwright-report'
const reportPath = reportBaseDir + '/index.html'
const uploadBaseDir = now.toISOString() + '/'

class UploadHtmlReporter implements Reporter {
  onBegin(_config: FullConfig, suite: Suite) {
    console.log(`Starting the run with ${suite.allTests().length} tests`)
  }

  async onEnd() {
    try {
      console.log('Generating HTML report...')

      const reportFilePath = path.resolve(reportPath)
      if (!fs.existsSync(reportFilePath)) {
        console.error('Report file not found:', reportFilePath)
        return
      }

      if (!uploadUsername) {
        console.error('REPORT_UPLOAD_USERNAME is not set')
        return
      }
      if (!uploadPassword) {
        console.error('REPORT_UPLOAD_PASSWORD is not set')
        return
      }

      const files = []

      files.push({ filename: uploadBaseDir + reportPath, content: fs.readFileSync(path.resolve(reportPath), 'utf8') })

      if (fs.existsSync(path.resolve(reportBaseDir + '/data'))) {
        fs.readdirSync(reportBaseDir + '/data').forEach((file) => {
          const filename = reportBaseDir + '/data/' + file
          const content = fs.readFileSync(path.resolve(filename)).toString('base64')
          files.push({ filename: uploadBaseDir + filename, content: content })
        })
      }

      await $fetch(uploadUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Basic ' + Buffer.from(uploadUsername + ':' + uploadPassword).toString('base64'),
        },
        body: JSON.stringify({ files: files }),
      })

      const link = uploadUrl + '/files/' + uploadBaseDir + reportPath

      console.log('Report uploaded successfully! ' + link)
    } catch (error) {
      console.error('Error uploading report:', (error as Error).message)
    }
  }
}

export default UploadHtmlReporter
