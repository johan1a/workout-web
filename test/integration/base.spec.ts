// @vitest-environment node
import { describe, expect, it } from 'vitest'
import { setup, $fetch } from '@nuxt/test-utils'

describe('/login', async () => {
  await setup({})

  it('should render login page', async () => {
    const html = await $fetch('/login')

    expect(html).toContain('Username')
    expect(html).toContain('Password')
  })

  it('should render workouts page', async () => {
    const html = await $fetch('/workouts')

    expect(html).toContain('Workouts')
  })

  it('should render exercises page', async () => {
    const html = await $fetch('/exercises')

    expect(html).toContain('Exercises')
  })

  it('should render programs page', async () => {
    const html = await $fetch('/programs')

    expect(html).toContain('Programs')
  })

  it('should render statistics page', async () => {
    const html = await $fetch('/statistics')

    expect(html).toContain('Statistics')
  })

  it('should render records page', async () => {
    const html = await $fetch('/records')

    expect(html).toContain('Records')
  })
})
