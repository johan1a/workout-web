import { flushPromises } from '@vue/test-utils'
import { afterEach, describe, it, expect, vi } from 'vitest'
import { createTestingPinia } from '@pinia/testing'
import CreateWorkout from '@/components/CreateWorkout.vue'
import { useProgramsStore } from '~/store/programs'
import { useWorkoutsStore } from '~/store/workouts'
import { mountWithSuspense } from '~/test/utils/test-utils'
import defaultWorkout from '~/test/testdata/workout0.json'
import defaultProgram from '~/test/testdata/program.json'
import type { Workout } from '~/models/workout'
import type { Program } from '~/models/program'

const navigateToMock = vi.fn()
vi.stubGlobal('navigateTo', navigateToMock)

createTestingPinia({
  stubActions: false,
  createSpy: vi.fn,
})

async function mountComponent() {
  return await mountWithSuspense(CreateWorkout, {})
}

describe('CreateWorkout', () => {
  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('program tab should be selected by default', async () => {
    function fakeFetch(url: string) {
      if (url === '/api/programs') {
        return { programs: [] }
      }
      return { workouts: [] }
    }
    const mock = vi.fn().mockImplementation(fakeFetch)
    vi.stubGlobal('$fetch', mock)
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const workoutsStore = useWorkoutsStore()
    const programsStore = useProgramsStore()
    workoutsStore.workouts = []
    programsStore.programs = []

    const wrapper = await mountComponent()

    const text = wrapper.text()

    expect(text).toContain('Create a workout from the selected program')
  })

  it('should create a new workout', async () => {
    function fakeFetch(url: string) {
      if (url === '/api/programs') {
        return { programs: [] }
      } else if (url === '/api/workouts/create') {
        return { id: 'the-workout-id' }
      }
    }
    const mock = vi.fn().mockImplementation(fakeFetch)
    vi.stubGlobal('$fetch', mock)
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const workoutsStore = useWorkoutsStore()
    const programsStore = useProgramsStore()
    workoutsStore.workouts = []
    programsStore.programs = []

    const wrapper = await mountComponent()

    await wrapper.get('#create-new-tab').trigger('click')
    await wrapper.get('#create-new-workout-button').trigger('click')
    await flushPromises()

    expect(navigateToMock).toBeCalledWith('/workouts/the-workout-id')
  })

  it('should copy workout', async () => {
    function fakeFetch(url: string) {
      if (url === '/api/programs') {
        return { programs: [] }
      } else if (url === '/api/workouts/create') {
        return { id: 'the-workout-id-new' }
      }
    }
    const mock = vi.fn().mockImplementation(fakeFetch)
    vi.stubGlobal('$fetch', mock)
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const workoutsStore = useWorkoutsStore()
    const programsStore = useProgramsStore()
    const originalWorkout: Workout = defaultWorkout
    workoutsStore.workouts = [originalWorkout]
    programsStore.programs = []

    const wrapper = await mountComponent()

    await wrapper.get('#copy-tab').trigger('click')
    await wrapper.get('#copy-workout-button').trigger('click')
    await flushPromises()

    expect(navigateToMock).toBeCalledWith('/workouts/the-workout-id-new')
  })

  it('should create a new workout from a program', async () => {
    function fakeFetch(url: string) {
      if (url === '/api/programs') {
        return { programs: [] }
      } else if (url === '/api/workouts/create') {
        return { id: 'the-workout-id-new' }
      }
    }
    const mock = vi.fn().mockImplementation(fakeFetch)
    vi.stubGlobal('$fetch', mock)
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const workoutsStore = useWorkoutsStore()
    const programsStore = useProgramsStore()
    workoutsStore.workouts = [defaultWorkout]
    programsStore.programs = [defaultProgram] as Array<Program>

    const wrapper = await mountComponent()

    await wrapper.get('#create-from-program-tab').trigger('click')
    await wrapper.get('#create-from-program-button').trigger('click')
    await flushPromises()

    expect(navigateToMock).toBeCalledWith('/workouts/the-workout-id-new')
  })
})
