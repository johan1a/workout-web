import { afterEach, describe, it, expect, vi } from 'vitest'
import { createTestingPinia } from '@pinia/testing'
import { mount } from '@vue/test-utils'
import AddWorkoutExercise from '@/components/AddWorkoutExercise.vue'
import defaultExercises from '~/test/testdata/recordExercises.json'
import { useProgramsStore } from '~/store/programs'
import { useExercisesStore } from '~/store/exercises'
import { useWorkoutsStore } from '~/store/workouts'
import WButton from '@/components/WButton.vue'

createTestingPinia({
  stubActions: false,
  createSpy: vi.fn,
})

describe('AddWorkoutExercise', () => {
  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('sort the list of available exercises', () => {
    function fakeFetch(url: string, _: any) {
      if (url === '/api/programs') {
        return { programs: [] }
      }
      return { workouts: [] }
    }
    const mock = vi.fn().mockImplementation(fakeFetch)
    vi.stubGlobal('$fetch', mock)
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const workoutsStore = useWorkoutsStore()
    const programsStore = useProgramsStore()
    const exercisesStore = useExercisesStore()
    workoutsStore.workouts = []
    programsStore.programs = []
    exercisesStore.exercises = defaultExercises.exercises

    const wrapper = mount(AddWorkoutExercise, {
      global: {
        components: {
          WButton,
        },
      },
    })

    const text = wrapper.findAll('option').map((option: any) => option.text())

    const expected = [
      'Select an exercise',
      'Armhävningar',
      'Bänkpress',
      'Knäböj',
      'Marklyft',
      'Militärpress',
      'Plankan',
      'Pull-ups',
      'Rygglyft',
      'Sittande rodd',
    ]

    expect(text).toEqual(expected)
  })
})
