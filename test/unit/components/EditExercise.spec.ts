import { afterEach, describe, it, expect, vi } from 'vitest'
import { createTestingPinia } from '@pinia/testing'
import { mount } from '@vue/test-utils'
import EditExercise from '@/components/EditExercise.vue'
import type { Exercise } from '~/models/exercise'
import WButton from '@/components/WButton.vue'

createTestingPinia({
  stubActions: false,
  createSpy: vi.fn,
})

describe('EditExercise', () => {
  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('should show a default header if no exercise prop is given', () => {
    const wrapper = mount(EditExercise, {
      global: {
        components: {
          WButton,
        },
      },
    })
    const text = wrapper.find('.exercise-header').text()

    expect(text).toEqual('Create new exercise')
  })

  it('should show the exercise name if one is given as prop', () => {
    const exercise: Exercise = {
      id: '123',
      name: 'Some press',
      exerciseType: 'some-type',
      userId: '456',
    }

    const wrapper = mount(EditExercise, {
      props: { exercise: exercise },
      global: {
        components: {
          WButton,
        },
      },
    })
    const text = wrapper.find('.exercise-header').text()

    expect(text).toEqual(exercise.name)
  })
})
