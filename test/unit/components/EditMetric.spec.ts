import { afterEach, describe, it, expect, vi } from 'vitest'
import { createTestingPinia } from '@pinia/testing'
import { mount } from '@vue/test-utils'
import EditMetric from '@/components/EditMetric.vue'
import type { MetricType } from '~/models/metricType'
import type { Metric } from '~/models/metric'
import WButton from '@/components/WButton.vue'

createTestingPinia({
  stubActions: false,
  createSpy: vi.fn,
})

function mountComponent(metricType: MetricType, metric: Metric) {
  return mount(EditMetric, {
    props: {
      metricType: metricType,
      metric: metric,
    },
    global: {
      components: {
        WButton,
        ErrorAlert: vi.fn(),
      },
    },
  })
}

describe('EditMetric', () => {
  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('should populate input fields from props', () => {
    const metricType: MetricType = {
      id: 'abc',
      userId: '90321',
      name: 'weight',
      minValue: -50,
      maxValue: 100,
    }
    const metric: Metric = {
      metricTypeId: 'abc',
      date: '2023-11-25',
      value: 12300,
      note: 'bla bla',
    }

    const wrapper = mountComponent(metricType, metric)

    const valueInput = wrapper.find('#input-value')
    expect((valueInput.element as HTMLInputElement).value).toEqual(String(metric.value / 100))

    const noteInput = wrapper.find('#input-note')
    expect((noteInput.element as HTMLInputElement).value).toEqual(String(metric.note))
  })
})
