import { afterEach, describe, it, expect, vi } from 'vitest'
import { createTestingPinia } from '@pinia/testing'
import { mount } from '@vue/test-utils'
import EditMetricType from '@/components/EditMetricType.vue'
import type { Metric } from '~/models/metric'
import type { MetricType } from '~/models/metricType'
import WButton from '@/components/WButton.vue'

createTestingPinia({
  stubActions: false,
  createSpy: vi.fn,
})

function mountComponent(metricType: MetricType, metric: Metric | undefined = undefined) {
  return mount(EditMetricType, {
    props: {
      metricType: metricType,
      metric: metric,
    },
    global: {
      components: {
        WButton,
      },
    },
  })
}

describe('EditMetric', () => {
  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('should populate input fields from props', () => {
    const metricType: MetricType = {
      id: 'abc',
      userId: '90321',
      name: 'weight',
      minValue: -50,
      maxValue: 100,
    }
    const wrapper = mountComponent(metricType)

    const nameInput = wrapper.find('#input-name')
    expect((nameInput.element as HTMLInputElement).value).toEqual(metricType.name)

    const minValueInput = wrapper.find('#input-min-value')
    expect((minValueInput.element as HTMLInputElement).value).toEqual(String(metricType.minValue!))

    const maxValueInput = wrapper.find('#input-max-value')
    expect((maxValueInput.element as HTMLInputElement).value).toEqual(String(metricType.maxValue!))
  })
})
