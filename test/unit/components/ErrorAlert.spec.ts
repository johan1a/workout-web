import { afterEach, describe, it, expect, vi } from 'vitest'
import { createTestingPinia } from '@pinia/testing'
import ErrorAlert from '@/components/ErrorAlert.vue'
import { mountWithSuspense } from '~/test/utils/test-utils'

createTestingPinia({
  stubActions: false,
  createSpy: vi.fn,
})

describe('ErrorAlert', () => {
  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('should have a header', async () => {
    vi.stubGlobal('$fetch', vi.fn())
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const wrapper = await mountWithSuspense(ErrorAlert, {
      props: { error: { message: 'Some error message' } },
      global: {
        plugins: [createTestingPinia({ stubActions: false, createSpy: vi.fn })],
      },
    })

    expect(wrapper.text()).toEqual('Some error message')
  })

  it('should show errors without message', async () => {
    vi.stubGlobal('$fetch', vi.fn())
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const error = { text: 'Some error message' }

    const wrapper = await mountWithSuspense(ErrorAlert, {
      props: { error: error },
      global: {
        plugins: [createTestingPinia({ stubActions: false, createSpy: vi.fn })],
      },
    })

    expect(wrapper.text()).toContain(error.text)
  })
})
