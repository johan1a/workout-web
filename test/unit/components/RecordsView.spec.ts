import { flushPromises } from '@vue/test-utils'
import { afterEach, describe, it, expect, vi } from 'vitest'
import { createTestingPinia } from '@pinia/testing'
import RecordsView from '@/components/RecordsView.vue'
import type { Exercise } from '~/models/exercise'
import type { ExerciseRecordData, RepMax } from '~/models/recordData'
import defaultRecordData from '~/test/testdata/records0.json'
import defaultExercises from '~/test/testdata/recordExercises.json'
import { useExercisesStore } from '~/store/exercises'
import { useRecordsStore } from '~/store/records'
import { mountWithSuspense } from '~/test/utils/test-utils'
import WButton from '@/components/WButton.vue'

const testingPinia = createTestingPinia({
  stubActions: false,
  createSpy: vi.fn,
})

async function mountComponent() {
  return await mountWithSuspense(RecordsView, {})
}

describe('RecordsView', () => {
  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('should have a header', async () => {
    vi.stubGlobal('$fetch', vi.fn())
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const wrapper = await mountWithSuspense(RecordsView, {
      global: {
        plugins: [createTestingPinia({ stubActions: false, createSpy: vi.fn })],
        components: {
          WButton,
        },
      },
    })

    expect(wrapper.get('h1').text()).toEqual('Records')
    expect(wrapper.get('#btn-recalculate').text()).toEqual('Force recalculation')
  })

  it('should show a table containing records', async () => {
    const recordsStore = useRecordsStore(testingPinia)
    const exerciseStore = useExercisesStore(testingPinia)

    const exercises = defaultExercises.exercises
    const recordData = defaultRecordData.records
    recordsStore.records = [recordData]
    exerciseStore.exercises = exercises

    const wrapper = await mountComponent()

    await flushPromises()

    const html = wrapper.get('#records-table').html()

    exercises.forEach((exercise: Exercise) => {
      const record = recordData.exerciseRecords.find(
        (r: ExerciseRecordData) => r.exerciseId === exercise.id && r.repMaxes.find((m) => m.weight > 0),
      )
      if (record) {
        expect(html).toContain(exercise.name)

        record.repMaxes.forEach((repMax: RepMax) => {
          expect(html).toContain(String(repMax.weight))
        })
      } else {
        expect(html).not.toContain(exercise.name)
      }
    })
  })

  it('should be possible to trigger recalculation', async () => {
    const fetchMock = vi.fn()
    vi.stubGlobal('$fetch', fetchMock)
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const wrapper = await mountComponent()

    wrapper.get('#btn-recalculate').trigger('click')

    await flushPromises()

    expect(fetchMock).toHaveBeenCalledWith('/api/records/recalculate', {
      headers: undefined,
      method: 'POST',
    })
  })
})
