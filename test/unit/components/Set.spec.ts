import { describe, it, expect, vi, beforeEach, afterEach } from 'vitest'
import { mount } from '@vue/test-utils'
import { createTestingPinia } from '@pinia/testing'
import WButton from '@/components/WButton.vue'
import SetMenu from '@/components/SetMenu.vue'
import IconMdiLightMoreVert from '@/components/icon/MdiLightMoreVert.vue'
import Set from '@/components/Set.vue'
import type { ExerciseSet } from '~/models/workout'

createTestingPinia({
  stubActions: false,
  createSpy: vi.fn,
})

const mockSet: ExerciseSet = { index: 3, weight: 50, reps: 10, checkedAt: undefined }

function mountComponent() {
  return mount(Set, {
    props: {
      exerciseId: '',
      workoutDate: '',
      set: mockSet,
      showWeight: true,
      showReps: true,
      showDuration: false,
    },
    global: {
      components: {
        WButton,
        Set,
        SetMenu,
        IconMdiLightMoreVert,
      },
      directives: {
        'click-outside': vi.fn(),
      },
    },
  })
}

describe('Set', () => {
  beforeEach(() => {
    vi.useFakeTimers()
  })

  afterEach(() => {
    vi.useRealTimers()
  })

  it('renders weight', () => {
    const wrapper = mountComponent()

    const element = wrapper.get('.input-set-weight').element as HTMLInputElement

    expect(element.value).toBe('50')
  })

  it('renders reps', () => {
    const wrapper = mountComponent()

    const element = wrapper.get('.input-set-reps').element as HTMLInputElement

    expect(element.value).toBe('10')
  })

  it('renders done', () => {
    const wrapper = mountComponent()

    const element = wrapper.get('.input-set-done').element as HTMLInputElement

    expect(element.checked).toBe(false)
  })

  it('emits update when weight changes', () => {
    const wrapper = mountComponent()

    const element = wrapper.get('.input-set-weight')

    element.setValue(12)

    vi.advanceTimersByTime(600)

    const emitted = (wrapper.emitted()['update-set'][0] as any)[0]

    expect(emitted).toEqual({ index: 3, weight: 12, reps: 10, checkedAt: undefined, durationSeconds: 0 })
  })

  it('emits update when reps change', () => {
    const wrapper = mountComponent()

    const element = wrapper.get('.input-set-reps')

    element.setValue(15)

    vi.advanceTimersByTime(600)

    const emitted = (wrapper.emitted()['update-set'][0] as any)[0]

    expect(emitted).toEqual({ index: 3, weight: 50, reps: 15, checkedAt: undefined, durationSeconds: 0 })
  })

  it('emits update when done is toggled', () => {
    const wrapper = mountComponent()

    const element = wrapper.get('.input-set-done')

    element.setValue('checked')

    vi.advanceTimersByTime(600)

    const checkedAt = (wrapper.emitted()['update-set'][0] as any)[0].checkedAt

    expect(checkedAt).toBeTruthy()
  })
})
