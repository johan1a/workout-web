import { describe, it, expect, vi, beforeEach, afterEach } from 'vitest'
import { createTestingPinia } from '@pinia/testing'
import { setActivePinia } from 'pinia'
import ShowProgram from '@/components/ShowProgram.vue'
import { type Exercise } from '@/models/exercise'
import { useProgramsStore } from '@/store/programs'
import { useExercisesStore } from '@/store/exercises'
import { mountWithSuspense } from '~/test/utils/test-utils'

const programId = 'program-id'

function mountComponent() {
  return mountWithSuspense(ShowProgram, {
    global: {
      components: {
        IconMdiLightPencil: vi.fn(),
        NuxtLink: vi.fn(),
      },
    },
  })
}

describe('ShowProgram.vue', () => {
  let programsStore: ReturnType<typeof useProgramsStore>
  let exercisesStore: ReturnType<typeof useExercisesStore>

  beforeEach(() => {
    vi.stubGlobal(
      'useRoute',
      vi.fn().mockReturnValue({
        params: { id: programId },
      }),
    )

    const pinia = createTestingPinia({ createSpy: vi.fn })
    setActivePinia(pinia)

    programsStore = useProgramsStore()
    exercisesStore = useExercisesStore()

    programsStore.programs = [
      {
        id: programId,
        name: 'Strength Program',
        data: {
          trainingMaxes: [
            {
              exerciseId: 'squat',
              weights: [100, 110, 120],
              defaultIncrement: 5,
            },
          ],
          weeks: [
            {
              index: 0,
              days: [
                {
                  index: 0,
                  exercises: [
                    {
                      exerciseId: 'squat',
                      index: 0,
                      sets: [
                        { index: 0, percentageOfMax: 80, nbrReps: 5, fixedWeight: null },
                        { index: 1, percentageOfMax: 85, nbrReps: 3, fixedWeight: null },
                      ],
                    },
                  ],
                },
              ],
            },
          ],
          currentRound: 0,
          currentDay: 0,
          currentWeek: 0,
          offsetMode: 0,
          createdWorkoutIds: [],
        },
        programType: 1,
        userId: '',
      },
    ]

    exercisesStore.findById = vi.fn((id: string) => {
      const exercise: Exercise = { id, name: id.charAt(0).toUpperCase() + id.slice(1), exerciseType: '', userId: '' }
      return exercise
    })
  })

  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('renders program name', async () => {
    const wrapper = await mountComponent()

    expect(wrapper.text()).toContain('Strength Program')
  })

  it('displays training maxes correctly', async () => {
    const wrapper = await mountComponent()

    expect(wrapper.text()).toContain('Squat')
    expect(wrapper.text()).toContain('120') // latest training max weight
    expect(wrapper.text()).toContain('5') // default increment
  })

  it('renders weeks and exercises', async () => {
    const wrapper = await mountComponent()

    expect(wrapper.text()).toContain('Week 1')
    expect(wrapper.text()).toContain('Day 1')
    expect(wrapper.text()).toContain('Squat')
    expect(wrapper.text()).toContain('97.5 x 5')
    expect(wrapper.text()).toContain('102.5 x 3')
  })
})
