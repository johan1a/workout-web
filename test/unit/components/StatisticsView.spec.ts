import { afterEach, describe, it, expect, vi } from 'vitest'
import { createTestingPinia } from '@pinia/testing'
import StatisticsView from '@/components/StatisticsView.vue'
import defaultExercises from '~/test/testdata/recordExercises.json'
import { useProgramsStore } from '~/store/programs'
import { useExercisesStore } from '~/store/exercises'
import { useWorkoutsStore } from '~/store/workouts'
import { mountWithSuspense } from '~/test/utils/test-utils'
import WButton from '@/components/WButton.vue'
import DateRangePicker from '@/components/DateRangePicker.vue'
import SelectButton from '@/components/SelectButton.vue'

createTestingPinia({
  stubActions: false,
  createSpy: vi.fn,
})

async function mountComponent() {
  return await mountWithSuspense(StatisticsView, {
    global: {
      components: {
        WButton,
        DateRangePicker,
        SelectButton,
        'client-only': vi.fn(),
      },
    },
  })
}

describe('StatisticsView', () => {
  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('sort the list of available exercises', async () => {
    function fakeFetch(url: string, _: any) {
      if (url === '/api/programs') {
        return { programs: [] }
      } else if (url === '/api/exercises') {
        return { exercises: defaultExercises.exercises }
      }
      return { workouts: [] }
    }
    const mock = vi.fn().mockImplementation(fakeFetch)
    vi.stubGlobal('$fetch', mock)
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const workoutsStore = useWorkoutsStore()
    const programsStore = useProgramsStore()
    const exercisesStore = useExercisesStore()
    workoutsStore.workouts = []
    programsStore.programs = []
    exercisesStore.exercises = defaultExercises.exercises

    const wrapper = await mountComponent()

    const text = wrapper.findAll('option').map((option: any) => option.text())

    const expected = [
      'Armhävningar',
      'Bänkpress',
      'Knäböj',
      'Marklyft',
      'Militärpress',
      'Plankan',
      'Pull-ups',
      'Rygglyft',
      'Sittande rodd',
      'Max weight',
      'Max set volume',
      'Max reps',
      'Max duration',
      'Total volume',
      'Total reps',
      'Total duration',
    ]

    expect(text).toEqual(expected)
  })
})
