import { mount } from '@vue/test-utils'
import { describe, it, expect } from 'vitest'
import WButton from '@/components/WButton.vue'

describe('WButton', () => {
  it('should have the correct default prop values', () => {
    const wrapper = mount(WButton, {})

    expect(wrapper.attributes().type).toEqual('button')
    expect(wrapper.attributes().disabled).toEqual(undefined)
  })
})
