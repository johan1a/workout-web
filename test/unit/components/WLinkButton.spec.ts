import { mount } from '@vue/test-utils'
import { describe, it, expect } from 'vitest'
import WLinkButton from '@/components/WLinkButton.vue'

describe('WButton', () => {
  it('should use the supplied link', () => {
    const wrapper = mount(WLinkButton, {
      props: { to: '/example' },
      global: {
        stubs: {
          NuxtLink: true,
        },
      },
    })

    expect(wrapper.attributes().to).toEqual('/example')
  })
})
