import { describe, it, expect, vi, beforeEach, afterEach } from 'vitest'
import { mount } from '@vue/test-utils'
import { createTestingPinia } from '@pinia/testing'
import WorkoutExerciseCard from '@/components/WorkoutExerciseCard.vue'
import WButton from '@/components/WButton.vue'
import AddSet from '@/components/AddSet.vue'
import SetMenu from '@/components/SetMenu.vue'
import IconMdiLightMoreVert from '@/components/icon/MdiLightMoreVert.vue'
import Set from '@/components/Set.vue'
import { useExercisesStore } from '~/store/exercises'
import type { ExerciseSet } from '~/models/workout'

createTestingPinia({
  stubActions: false,
  createSpy: vi.fn,
})

const mockWorkout = {
  date: '2024-02-05',
  id: '',
  data: {
    exercises: [],
  },
  programId: '',
  userId: '',
  version: 0,
}

const mockWorkoutExercise = {
  index: 1,
  exerciseId: 'exercise-1',
  sets: [
    { index: 1, weight: 50, reps: 10, duration: null },
    { index: 2, weight: 60, reps: 8, duration: null },
  ],
}

function mountComponent() {
  return mount(WorkoutExerciseCard, {
    props: {
      workout: mockWorkout,
      workoutExercise: mockWorkoutExercise,
    },
    global: {
      components: {
        WButton,
        AddSet,
        Set,
        SetMenu,
        IconMdiLightMoreVert,
      },
      directives: {
        'click-outside': vi.fn(),
      },
    },
  })
}

describe('ExerciseComponent', () => {
  const exercisesStore = useExercisesStore()

  beforeEach(() => {
    exercisesStore.findById = vi.fn(() => ({ id: '', userId: '', exerciseType: 'repsAndWeight', name: 'Bench Press' }))
    vi.useFakeTimers()
  })

  afterEach(() => {
    vi.useRealTimers()
  })

  it('renders exercise name', () => {
    const wrapper = mountComponent()
    expect(wrapper.text()).toContain('Bench Press')
  })

  it('shows delete button in edit mode', async () => {
    const wrapper = mountComponent()
    const checkbox = wrapper.get('input[name="edit-mode-button"]')
    await checkbox.setValue(true)

    const button = wrapper.get('.button-delete-workout-exercise')

    expect(button.text()).toBe('Delete')
  })

  it('emits update-workout-set when changing reps', () => {
    const wrapper = mountComponent()

    const input = wrapper.get('.input-set-reps')
    input.setValue(12)
    vi.advanceTimersByTime(600)

    const emitted = wrapper.emitted()['update-workout-set']

    expect(emitted[0]).toEqual([1, { index: 1, weight: 50, reps: 12, checkedAt: undefined, durationSeconds: 0 }])
  })

  it('emits update-workout-set when changing weight', () => {
    const wrapper = mountComponent()

    const input = wrapper.get('.input-set-weight')
    input.setValue(57.5)
    vi.advanceTimersByTime(600)

    const emitted = wrapper.emitted()['update-workout-set']

    expect(emitted[0]).toEqual([1, { index: 1, weight: 57.5, reps: 10, checkedAt: undefined, durationSeconds: 0 }])
  })

  it('emits update-workout-set when toggling done', () => {
    const wrapper = mountComponent()

    const input = wrapper.get('.input-set-done')
    input.setValue(true)
    vi.advanceTimersByTime(600)

    const emitted = (wrapper.emitted()['update-workout-set'][0] as any)[1] as ExerciseSet

    expect(emitted).toBeTruthy()
  })
})
