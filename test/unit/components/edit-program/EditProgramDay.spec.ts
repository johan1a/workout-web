import { describe, it, expect, vi, beforeEach, afterEach } from 'vitest'
import { mount } from '@vue/test-utils'
import { setActivePinia, createPinia } from 'pinia'
import EditProgramDay from '@/components/edit-program/EditProgramDay.vue'
import EditProgramExercise from '@/components/edit-program/EditProgramExercise.vue'
import WButton from '@/components/WButton.vue'
import SelectExercise from '@/components/SelectExercise.vue'
import IconMdiArrowUp from '@/components/icon/MdiArrowUp.vue'
import IconMdiLightPlusCircle from '@/components/icon/MdiLightPlusCircle.vue'
import IconMdiArrowDown from '@/components/icon/MdiArrowDown.vue'
import IconMdiClose from '@/components/icon/MdiClose.vue'

function mountComponent() {
  return mount(EditProgramDay, {
    props: {
      day: {
        index: 0,
        exercises: [
          { index: 0, sets: [], exerciseId: 'exercise-0' },
          { index: 1, sets: [], exerciseId: 'exercise-1' },
        ],
      },
    },
    global: {
      components: {
        WButton,
        SelectExercise,
        IconMdiArrowUp,
        IconMdiArrowDown,
        IconMdiClose,
        IconMdiLightPlusCircle,
        EditProgramExercise,
      },
      stubs: {
        EditProgramSet: true,
      },
    },
  })
}

describe('EditProgramDay', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
  })

  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('renders the day index', () => {
    const wrapper = mountComponent()
    expect(wrapper.text()).toContain('Day 1')
  })

  it('emits move-day-up event', async () => {
    const wrapper = mountComponent()
    await wrapper.find('.button-move-day-up').trigger('click')
    expect(wrapper.emitted()['move-day-up'][0]).toEqual([0])
  })

  it('emits move-day-down event', async () => {
    const wrapper = mountComponent()
    await wrapper.find('.button-move-day-down').trigger('click')
    expect(wrapper.emitted()['move-day-down'][0]).toEqual([0])
  })

  it('emits remove-day event', async () => {
    const wrapper = mountComponent()
    await wrapper.find('.button-remove-day').trigger('click')
    expect(wrapper.emitted()['remove-day'][0]).toEqual([0])
  })

  it('adds an exercise when Add Exercise button is clicked', async () => {
    const wrapper = mountComponent()

    await wrapper.find('.button-add-exercise').trigger('click')

    expect(wrapper.vm.day.exercises.length).toBe(3)
  })

  it('updates exercise when update-exercise event is emitted', async () => {
    const wrapper = mountComponent()

    await wrapper.find('.select-program-exercise').setValue('bench-press')

    const emitted = (wrapper.emitted()['update-day'][0] as any)[0]

    expect(emitted).toEqual({
      index: 0,
      exercises: [
        { index: 0, sets: [], exerciseId: undefined },
        { index: 1, sets: [], exerciseId: 'exercise-1' },
      ],
    })
  })

  it('handles move-exercise-down event', async () => {
    const wrapper = mountComponent()

    await wrapper.find('.button-move-exercise-down').trigger('click')

    const emitted = (wrapper.emitted()['update-day'][0] as any)[0]

    expect(emitted).toEqual({
      exercises: [
        {
          exerciseId: 'exercise-1',
          index: 0,
          sets: [],
        },
        {
          exerciseId: 'exercise-0',
          index: 1,
          sets: [],
        },
      ],
      index: 0,
    })
  })

  it('handles move-exercise-up event', async () => {
    const wrapper = mountComponent()

    await wrapper.findAll('.button-move-exercise-up').at(1)!.trigger('click')

    const emitted = (wrapper.emitted()['update-day'][0] as any)[0]

    expect(emitted).toEqual({
      exercises: [
        {
          exerciseId: 'exercise-1',
          index: 0,
          sets: [],
        },
        {
          exerciseId: 'exercise-0',
          index: 1,
          sets: [],
        },
      ],
      index: 0,
    })
  })

  it('emits remove-exercise event', async () => {
    const wrapper = mountComponent()

    await wrapper.find('.button-remove-exercise').trigger('click')

    const emitted = (wrapper.emitted()['update-day'][0] as any)[0]

    expect(emitted).toEqual({
      exercises: [
        {
          exerciseId: 'exercise-1',
          index: 0,
          sets: [],
        },
      ],
      index: 0,
    })
  })
})
