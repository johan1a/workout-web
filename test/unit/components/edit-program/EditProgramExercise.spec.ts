import { describe, it, expect, vi, beforeEach, afterEach } from 'vitest'
import { mount } from '@vue/test-utils'
import { setActivePinia, createPinia } from 'pinia'
import EditProgramExercise from '@/components/edit-program/EditProgramExercise.vue'
import SelectExercise from '@/components/SelectExercise.vue'
import EditProgramSet from '@/components/edit-program/EditProgramSet.vue'
import IconMdiClose from '@/components/icon/MdiClose.vue'
import IconMdiLightPlusCircle from '@/components/icon/MdiLightPlusCircle.vue'
import IconMdiArrowUp from '@/components/icon/MdiArrowUp.vue'
import IconMdiArrowDown from '@/components/icon/MdiArrowDown.vue'

const exerciseData = {
  index: 0,
  exerciseId: 'squat',
  sets: [
    { index: 0, percentageOfMax: 80, fixedWeight: null, nbrReps: 5 },
    { index: 1, percentageOfMax: 85, fixedWeight: null, nbrReps: 3 },
  ],
}

function mountComponent() {
  return mount(EditProgramExercise, {
    global: {
      components: {
        SelectExercise,
        EditProgramSet,
        IconMdiClose,
        IconMdiLightPlusCircle,
        IconMdiArrowUp,
        IconMdiArrowDown,
      },
    },
    props: { exercise: exerciseData },
  })
}

describe('EditProgramExercise.vue', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
  })

  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('renders exercise selection', () => {
    const wrapper = mountComponent()
    expect(wrapper.find('.select-program-exercise').exists()).toBe(true)
  })

  it('renders sets correctly', () => {
    const wrapper = mountComponent()
    const sets = wrapper.findAllComponents(EditProgramSet)
    expect(sets).toHaveLength(2)
  })

  it('emits update-exercise when modifying exercise', async () => {
    const wrapper = mountComponent()
    await wrapper.find('.select-program-exercise').setValue('bench-press')
    const emitted = wrapper.emitted()['update-exercise']
    expect(emitted).toBeTruthy()
  })

  it('adds a new set correctly', async () => {
    const wrapper = mountComponent()
    await wrapper.find('.button-add-set').trigger('click')
    expect(wrapper.findAllComponents(EditProgramSet)).toHaveLength(3)
    const emitted = wrapper.emitted()['update-exercise']
    expect(emitted).toEqual([
      [
        {
          index: 0,
          exerciseId: 'squat',
          sets: [
            { index: 0, percentageOfMax: 80, fixedWeight: null, nbrReps: 5 },
            { index: 1, percentageOfMax: 85, fixedWeight: null, nbrReps: 3 },
            { index: 2, percentageOfMax: 85, fixedWeight: null, nbrReps: 3 },
          ],
        },
      ],
    ])
  })

  it('removes a set correctly', async () => {
    const wrapper = mountComponent()
    await wrapper.find('.button-remove-set').trigger('click')
    expect(wrapper.findAllComponents(EditProgramSet)).toHaveLength(1)
  })

  it('emits move-exercise-up event', async () => {
    const wrapper = mountComponent()
    await wrapper.find('.button-move-exercise-up').trigger('click')
    expect(wrapper.emitted()['move-exercise-up']).toBeTruthy()
  })

  it('emits move-exercise-down event', async () => {
    const wrapper = mountComponent()
    await wrapper.find('.button-move-exercise-down').trigger('click')
    expect(wrapper.emitted()['move-exercise-down']).toBeTruthy()
  })

  it('emits remove-exercise event', async () => {
    const wrapper = mountComponent()
    await wrapper.find('.button-remove-exercise').trigger('click')
    expect(wrapper.emitted()['remove-exercise']).toBeTruthy()
  })
})
