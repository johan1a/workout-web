import { describe, it, expect, vi, afterEach } from 'vitest'
import { mount } from '@vue/test-utils'
import { type ProgramSet } from '@/models/program'
import EditProgramSet from '@/components/edit-program/EditProgramSet.vue'
import IconMdiClose from '@/components/icon/MdiClose.vue'

const mockSet = {
  index: 1,
  percentageOfMax: 80,
  fixedWeight: null,
  nbrReps: 5,
}

function mountComponent() {
  return mount(EditProgramSet, {
    props: { set: { ...mockSet } },
    global: {
      components: {
        IconMdiClose,
      },
    },
  })
}

describe('EditProgramSet.vue', () => {
  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('renders set details correctly', () => {
    const wrapper = mountComponent()
    expect(wrapper.find('.input-set-percentage-of-max').exists()).toBe(true)
    expect(wrapper.find('.input-set-nbr-reps').exists()).toBe(true)
    expect(wrapper.find('.select-program-set-type').exists()).toBe(true)
  })

  it('updates percentage of max correctly', async () => {
    const wrapper = mountComponent()
    const input = wrapper.find('.input-set-percentage-of-max')
    await input.setValue(85)
    expect(wrapper.emitted('update-set')).toBeTruthy()
    expect(getEmitted(wrapper, 'update-set').percentageOfMax).toBe(85)
  })

  it('updates fixed weight correctly', async () => {
    const wrapper = mountComponent()
    await wrapper.setProps({ set: { ...mockSet, percentageOfMax: null, fixedWeight: 100 } })
    const input = wrapper.find('.input-set-fixed-weight')
    await input.setValue(105)
    expect(wrapper.emitted('update-set')).toBeTruthy()
    expect(getEmitted(wrapper, 'update-set').fixedWeight).toBe(105)
  })

  it('changes set type correctly', async () => {
    const wrapper = mountComponent()
    const select = wrapper.find('.select-program-set-type')
    await select.setValue('FIXED_WEIGHT')
    const emittedSet = getEmitted(wrapper, 'update-set')
    expect(emittedSet.fixedWeight).toBe(80)
    expect(emittedSet.percentageOfMax).toBeNull()
  })

  it('removes a set when clicking remove button', async () => {
    const wrapper = mountComponent()
    const removeButton = wrapper.find('.button-remove-set')
    await removeButton.trigger('click')
    expect(wrapper.emitted('remove-set')).toBeTruthy()
    expect(getEmitted(wrapper, 'remove-set')).toBe(mockSet.index)
  })

  it('updates reps weight correctly', async () => {
    const wrapper = mountComponent()
    await wrapper.setProps({ set: { ...mockSet, percentageOfMax: null, fixedWeight: 100 } })
    const input = wrapper.find('.input-set-fixed-weight')
    await input.setValue(105)
    expect(wrapper.emitted('update-set')).toBeTruthy()
    expect(getEmitted(wrapper, 'update-set').nbrReps).toBe(5)
  })

  function getEmitted(wrapper: any, eventType: string): ProgramSet {
    return wrapper.emitted(eventType)[0][0] as ProgramSet
  }
})
