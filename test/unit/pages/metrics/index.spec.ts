import { afterEach, describe, it, expect, vi } from 'vitest'
import { createTestingPinia } from '@pinia/testing'
import { mountWithSuspense } from '~/test/utils/test-utils'
import MetricsIndexPage from '@/pages/metrics/index.vue'

createTestingPinia({
  stubActions: false,
  createSpy: vi.fn,
})

describe('/metrics/index', () => {
  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('should link to the register metrics page', async () => {
    vi.stubGlobal(
      'useRoute',
      vi.fn().mockReturnValue({
        query: { selectedMetricTypeId: 'some-id' },
      }),
    )
    vi.stubGlobal('useRouter', vi.fn().mockReturnValue({}))

    const wrapper = await mountWithSuspense(MetricsIndexPage, {
      props: {},
      global: {
        stubs: {
          DateRangePicker: true,
          NuxtLink: true,
        },
        components: {
          'client-only': {
            template: '<slot />',
          },
        },
      },
    })

    const link = wrapper.find('#registerLink').attributes().to

    expect(link).toEqual('/metric-types/some-id/register')
  })
})
