import { afterEach, describe, it, expect, vi } from 'vitest'
import { createTestingPinia } from '@pinia/testing'
import WorkoutsIdPage from '@/pages/workouts/[id].vue'
import { useProgramsStore } from '~/store/programs'
import { useWorkoutsStore } from '~/store/workouts'
import { mountWithSuspense } from '~/test/utils/test-utils'
import defaultWorkout from '~/test/testdata/workout0.json'

createTestingPinia({
  stubActions: false,
  createSpy: vi.fn,
})

async function mountComponent() {
  return await mountWithSuspense(WorkoutsIdPage, {
    props: {},
    global: {
      stubs: {
        IconMdiLightPencil: true,
        EditDateModal: true,
        ErrorAlert: true,
        WorkoutExerciseCard: true,
        AddWorkoutExercise: true,
      },
    },
  })
}

describe('/workouts/[id]', () => {
  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('should show workout date', async () => {
    function fakeFetch(url: string, _: any) {
      if (url === '/api/programs') {
        return { programs: [] }
      }
      return { workouts: [] }
    }

    vi.stubGlobal(
      'useRoute',
      vi.fn().mockReturnValue({
        params: { id: defaultWorkout.id },
      }),
    )
    const mock = vi.fn().mockImplementation(fakeFetch)
    vi.stubGlobal('$fetch', mock)
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const workoutsStore = useWorkoutsStore()
    const programsStore = useProgramsStore()
    workoutsStore.workouts = [defaultWorkout]
    programsStore.programs = []

    const wrapper = await mountComponent()

    const text = wrapper.text()

    expect(text).toEqual('2022-01-10')
  })
})
