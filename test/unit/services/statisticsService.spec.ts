import { describe, expect, it } from 'vitest'
import type { Workout } from '~/models/workout'
import { statisticsService, maxSetVolume, maxWeight, totalVolume } from '~/services/statisticsService'
import defaultWorkouts from '~/test/testdata/workouts.json'

describe('statisticsService', () => {
  it('should calculate max weights per workout', () => {
    const workouts = defaultWorkouts as Array<Workout>
    const exerciseId = '33585562-be61-436d-a213-4db87d5be023'

    const expected = [[15, 25, 200], [], ['2022-01-20', '2022-03-21', '2022-01-06']]

    expect(statisticsService.getMaxes(workouts, maxWeight, exerciseId)).toEqual(expected)
  })

  it('should calculate max set volume per workout', () => {
    const workouts = defaultWorkouts as Array<Workout>
    const exerciseId = '33585562-be61-436d-a213-4db87d5be023'

    const expected = [
      [150, 250, 300],
      ['10 x 15', '10 x 25', '3 x 100'],
      ['2022-01-20', '2022-03-21', '2022-01-06'],
    ]

    expect(statisticsService.getMaxes(workouts, maxSetVolume, exerciseId)).toEqual(expected)
  })

  it('should calculate total volume per workout', () => {
    const workouts = defaultWorkouts as Array<Workout>
    const exerciseId = '33585562-be61-436d-a213-4db87d5be023'

    const expected = [
      [300, 750, 750],
      ['2022-01-20', '2022-03-21', '2022-01-06'],
    ]

    expect(statisticsService.getTotal(workouts, totalVolume, exerciseId)).toEqual(expected)
  })

  it('should not include first set if it is unchecked', () => {
    const workouts: Array<Workout> = [
      {
        data: {
          exercises: [
            {
              exerciseId: '33585562-be61-436d-a213-4db87d5be023',
              index: 4,
              sets: [
                {
                  durationSeconds: 0,
                  index: 0,
                  reps: 10,
                  weight: 200.0,
                },
                {
                  checkedAt: '2022-01-20T14:26',
                  durationSeconds: 0,
                  index: 1,
                  reps: 10,
                  weight: 10.0,
                },
              ],
            },
          ],
        },
        date: '2022-01-20',
        id: '6371238f-1a6b-4a13-904d-8ede65afa833',
        userId: '123',
        version: 14,
        programId: 'N/A',
      },
    ]
    const exerciseId = '33585562-be61-436d-a213-4db87d5be023'

    const expected = [[10], [], ['2022-01-20']]

    expect(statisticsService.getMaxes(workouts, maxWeight, exerciseId)).toEqual(expected)
  })
})
