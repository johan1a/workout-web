import { afterEach, describe, expect, it, vi } from 'vitest'
import { userService } from '~/services/userService'

describe('exercises', () => {
  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('should redirect to /login after logging out', async () => {
    const navigateMock = vi.fn()

    vi.stubGlobal('$fetch', vi.fn().mockResolvedValue({ message: 'alright' }))
    vi.stubGlobal('navigateTo', navigateMock)
    vi.stubGlobal('useRequestHeaders', vi.fn())

    await userService.logout()

    expect(navigateMock).toBeCalledWith('/login')
  })

  it('should redirect to /login after logging out when there is an error', async () => {
    const navigateMock = vi.fn()

    const errorMessage = 'Some error'
    vi.stubGlobal('$fetch', vi.fn().mockRejectedValue(errorMessage))
    vi.stubGlobal('navigateTo', navigateMock)
    vi.stubGlobal('useRequestHeaders', vi.fn())

    await userService.logout()

    expect(navigateMock).toBeCalledWith('/login')
  })
})
