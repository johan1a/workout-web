import { setActivePinia, createPinia } from 'pinia'
import { beforeEach, afterEach, describe, expect, it, vi } from 'vitest'
import type { Exercise } from '~/models/exercise'
import { useExercisesStore } from '~/store/exercises'

describe('exercises', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
  })

  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('exercises should be falsy if there are no Exercises', () => {
    const exercisesStore = useExercisesStore()

    const actual = exercisesStore.exercises

    expect(actual).toEqual([])
  })

  it('fetchExercises() should fetch Exercises from the server', async () => {
    const exercisesStore = useExercisesStore()

    const response = {
      exercises: [
        {
          name: 'Deadlift',
          id: '12346',
          exerciseType: 'duration',
          userId: '4643634',
        },
        {
          name: 'Bench press',
          id: '12345',
          exerciseType: 'repsAndWeight',
          userId: '4643634',
        },
      ],
    }
    vi.stubGlobal('$fetch', vi.fn().mockResolvedValue(response))
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const actual = await exercisesStore.fetchExercises()

    expect(actual).toEqual(response.exercises)
    expect(exercisesStore.exercises).toEqual(response.exercises)
  })

  it('save() should save Exercises', async () => {
    const exercisesStore = useExercisesStore()

    const exercise: Exercise = {
      name: 'Bench press',
      id: '12345',
      exerciseType: 'repsAndWeight',
      userId: '4643634',
    }

    vi.stubGlobal('$fetch', vi.fn().mockResolvedValue(exercise))
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const actual = await exercisesStore.save(exercise)

    expect(actual).toEqual(exercise)
    expect(exercisesStore.exercises[0]).toEqual(exercise)
  })

  it('fetchExercises() should navigate to /login if there is a 403 error', async () => {
    const exercisesStore = useExercisesStore()
    const navigateMock = vi.fn()

    const error = new Error('403 talk 2 da hand!')

    vi.stubGlobal('$fetch', vi.fn().mockRejectedValue(error))
    vi.stubGlobal('useRequestHeaders', vi.fn())
    vi.stubGlobal('navigateTo', navigateMock)

    await exercisesStore.fetchExercises()

    expect(navigateMock).toBeCalledWith('/login')
  })

  it('fetchExercises() should return errors', async () => {
    const exercisesStore = useExercisesStore()
    const navigateMock = vi.fn()

    const errorMessage = 'They set up us the bomb!'
    vi.stubGlobal('$fetch', vi.fn().mockRejectedValue(errorMessage))
    vi.stubGlobal('useRequestHeaders', vi.fn())
    vi.stubGlobal('navigateTo', navigateMock)

    const actual = await exercisesStore.fetchExercises()

    expect(actual).toEqual({ message: errorMessage })
  })
})
