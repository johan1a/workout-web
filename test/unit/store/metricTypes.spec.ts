import { setActivePinia, createPinia } from 'pinia'
import { beforeEach, afterEach, describe, expect, it, vi } from 'vitest'
import type { MetricType } from '~/models/metricType'
import { useMetricTypesStore } from '~/store/metricTypes'

describe('metricTypesStore', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
  })

  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('getMetrics() should be falsy if there are no MetricTypes', () => {
    const metricTypesStore = useMetricTypesStore()

    const actual = metricTypesStore.list

    expect(actual).toEqual([])
  })

  it('fetchMetricTypes() should fetch MetricTypes from the server', async () => {
    const metricTypesStore = useMetricTypesStore()

    const response: Array<MetricType> = [
      {
        id: '0',
        userId: 'hey',
        name: '1111',
        minValue: null,
        maxValue: 10,
      },
      {
        id: '1',
        userId: 'hey',
        name: '2222',
        minValue: -1,
        maxValue: null,
      },
    ]
    vi.stubGlobal('$fetch', vi.fn().mockResolvedValue(response))
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const actual = await metricTypesStore.fetchMetricTypes()

    expect(actual).toEqual(response)
  })

  it('findById() should find MetricTypes by id', async () => {
    const metricTypesStore = useMetricTypesStore()

    const response: Array<MetricType> = [
      {
        id: '0',
        userId: 'hey',
        name: '1111',
        minValue: null,
        maxValue: 10,
      },
      {
        id: '1',
        userId: 'hey',
        name: '2222',
        minValue: -1,
        maxValue: null,
      },
    ]
    vi.stubGlobal('$fetch', vi.fn().mockResolvedValue(response))
    vi.stubGlobal('useRequestHeaders', vi.fn())

    await metricTypesStore.fetchMetricTypes()
    const actual = await metricTypesStore.findById('1')

    expect(actual).toEqual(response[1])
  })

  it('save() should save MetricTypes', async () => {
    const metricTypesStore = useMetricTypesStore()

    const metricType: MetricType = {
      id: '3',
      userId: 'heyyy',
      name: '3333',
      minValue: -1,
      maxValue: null,
    }

    vi.stubGlobal('$fetch', vi.fn().mockResolvedValue(metricType))
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const actual = await metricTypesStore.save(metricType)

    expect(actual).toEqual(metricType)
    expect(metricTypesStore.list).toEqual([metricType])
  })
})
