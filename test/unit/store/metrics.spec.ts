import { setActivePinia, createPinia } from 'pinia'
import { beforeEach, afterEach, describe, expect, it, vi } from 'vitest'
import type { Metric } from '~/models/metric'
import { useMetricsStore } from '~/store/metrics'

describe('metricsStore', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
  })

  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('getMetrics() should be falsy if there are no Metrics', () => {
    const metricsStore = useMetricsStore()

    const actual = metricsStore.getMetrics

    expect(actual).toEqual(new Map())
  })

  it('fetchMetrics() should fetch Metrics from the server', async () => {
    const metricsStore = useMetricsStore()

    const response: Array<Metric> = [
      {
        metricTypeId: '12309',
        date: '2023-11-25',
        value: 82391,
        note: 'hoabanoebaon',
      },
      {
        metricTypeId: '12309',
        date: '2023-11-26',
        value: 82390,
        note: 'yiiiiiiii',
      },
    ]
    vi.stubGlobal('$fetch', vi.fn().mockResolvedValue(response))
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const actual = await metricsStore.fetchMetrics('12309')

    expect(actual).toEqual(response)
  })

  it('save() should save Metrics', async () => {
    const metricsStore = useMetricsStore()

    const metric: Metric = {
      metricTypeId: 'wababababa',
      date: '2023-11-25',
      value: 3333,
      note: 'buy milk',
    }

    vi.stubGlobal('$fetch', vi.fn().mockResolvedValue(metric))
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const actual = await metricsStore.save(metric)

    expect(actual).toEqual(metric)
    expect(metricsStore.getMetrics.get('wababababa')).toEqual([metric])
  })
})
