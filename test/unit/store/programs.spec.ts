import { setActivePinia, createPinia } from 'pinia'
import { beforeEach, afterEach, describe, expect, it, vi } from 'vitest'
import { useProgramsStore } from '~/store/programs'

describe('programsStore', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
  })

  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('store.programs should not be undefined', () => {
    const programsStore = useProgramsStore()

    expect(programsStore.programs).toEqual([])
  })
})
