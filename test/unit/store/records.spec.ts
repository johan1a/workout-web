import { setActivePinia, createPinia } from 'pinia'
import { beforeEach, afterEach, describe, expect, it, vi } from 'vitest'
import type { WorkoutExercise } from '~/models/workout'
import { RecordTypeReps } from '~/models/workout'
import { useRecordsStore } from '~/store/records'

describe('records', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
  })

  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('should return text and emoji for an exercise with a record', () => {
    const recordsStore = useRecordsStore()

    const exerciseId = '3db5ce97-505b-4cb7-bdaa-13e16b547b20'
    const date = '2022-06-29'

    const recordData = {
      userId: '',
      exerciseRecords: [
        {
          exerciseId: exerciseId,
          repMaxes: [
            {
              index: 0,
              nbrReps: 5,
              weight: 80,
              date: date,
            },
          ],
        },
      ],
    }
    recordsStore.records = [recordData]

    const exercise: WorkoutExercise = {
      exerciseId: exerciseId,
      index: 0,
      sets: [
        {
          index: 0,
          reps: 5,
          weight: 80,
          checkedAt: '123',
        },
      ],
    }

    const actual = recordsStore.getExerciseRecordText(exercise, date)

    expect(actual).toBe('🍾 5RM')
  })

  it('should return text for an exercise with multiple records', () => {
    const recordsStore = useRecordsStore()

    const exerciseId = '3db5ce97-505b-4db7-bdaa-13e16b547b20'
    const date = '2022-06-29'

    const recordData = {
      userId: '',
      exerciseRecords: [
        {
          exerciseId: exerciseId,
          repMaxes: [
            {
              index: 0,
              nbrReps: 5,
              weight: 80,
              date: date,
            },
            {
              index: 1,
              nbrReps: 3,
              weight: 40,
              date: date,
            },
          ],
        },
      ],
    }
    recordsStore.records = [recordData]

    const exercise: WorkoutExercise = {
      exerciseId: exerciseId,
      index: 0,
      sets: [
        {
          index: 0,
          reps: 5,
          weight: 80,
          recordMarkers: { recordType: RecordTypeReps },
          checkedAt: '123',
        },
        {
          index: 1,
          reps: 3,
          weight: 40,
          recordMarkers: { recordType: RecordTypeReps },
          checkedAt: '123',
        },
      ],
    }

    const actual = recordsStore.getExerciseRecordText(exercise, date)

    expect(actual).toBe('🍾 5RM, ✨ 3RM')
  })

  it('should not return a text for an exercise that is unchecked', () => {
    const recordsStore = useRecordsStore()

    const exerciseId = '3db6ce97-505b-4cb7-bdaa-13e16b547b20'
    const date = '2022-06-29'

    const recordData = {
      userId: '',
      exerciseRecords: [
        {
          exerciseId: exerciseId,
          repMaxes: [
            {
              index: 0,
              nbrReps: 5,
              weight: 80,
              date: date,
            },
          ],
        },
      ],
    }
    recordsStore.records = [recordData]

    const exercise: WorkoutExercise = {
      exerciseId: exerciseId,
      index: 0,
      sets: [
        {
          index: 0,
          reps: 5,
          weight: 80,
        },
      ],
    }

    const actual = recordsStore.getExerciseRecordText(exercise, date)

    expect(actual).toBe('')
  })

  it('should not return a text for an exercise without a record', () => {
    const recordsStore = useRecordsStore()

    const date = '2022-06-29'
    const exercise: WorkoutExercise = {
      exerciseId: '',
      index: 0,
      sets: [
        {
          index: 0,
          reps: 5,
          weight: 80,
          checkedAt: '123',
        },
      ],
    }

    const actual = recordsStore.getExerciseRecordText(exercise, date)

    expect(actual).toBe('')
  })

  it('should return text for a set with a record', () => {
    const recordsStore = useRecordsStore()

    const exerciseId = '53eaa6a3-471b-4fb4-98e5-134835510f8c'
    const date = '2022-06-29'
    const recordData = {
      userId: '',
      exerciseRecords: [
        {
          exerciseId: exerciseId,
          repMaxes: [
            {
              index: 0,
              nbrReps: 5,
              weight: 80,
              date: date,
            },
          ],
        },
      ],
    }
    recordsStore.records = [recordData]

    const set = {
      index: 0,
      reps: 5,
      weight: 80,
      checkedAt: '123',
    }

    const actual = recordsStore.getSetRecordText(exerciseId, date, set)

    expect(actual).toBe('5RM')
  })

  it('should not return text for a set without a record', () => {
    const recordsStore = useRecordsStore()

    const exerciseId = '53eaa6a3-471b-4fb4-98e5-134835510f8d'
    const date = '2022-06-29'
    const set = {
      index: 0,
      reps: 5,
      weight: 80,
      checkedAt: '123',
    }

    const actual = recordsStore.getSetRecordText(exerciseId, date, set)

    expect(actual).toBe('')
  })

  it('should not return text for an unchecked set', () => {
    const recordsStore = useRecordsStore()

    const exerciseId = '53eaa6a3-471b-4fb4-98e5-134835510f9d'
    const date = '2022-06-29'
    const recordData = {
      userId: '',
      exerciseRecords: [
        {
          exerciseId: exerciseId,
          repMaxes: [
            {
              index: 0,
              nbrReps: 5,
              weight: 80,
              date: date,
            },
          ],
        },
      ],
    }
    recordsStore.records = [recordData]

    const set = {
      index: 0,
      reps: 5,
      weight: 80,
    }

    const actual = recordsStore.getSetRecordText(exerciseId, date, set)

    expect(actual).toBe('')
  })

  it('should return an emoji for a set with a record', () => {
    const recordsStore = useRecordsStore()

    const exerciseId = '53eaa6a3-471b-4fb4-98e5-234835510f9d'
    const date = '2022-06-29'
    const recordData = {
      userId: '',
      exerciseRecords: [
        {
          exerciseId: exerciseId,
          repMaxes: [
            {
              index: 0,
              nbrReps: 5,
              weight: 80,
              date: date,
            },
          ],
        },
      ],
    }
    recordsStore.records = [recordData]

    const set = {
      index: 0,
      reps: 5,
      weight: 80,
      checkedAt: '123',
    }

    const actual = recordsStore.getSetRecordEmoji(exerciseId, date, set)

    expect(actual).toBe('🍾')
  })

  it('should not return an emoji for a set without a record', () => {
    const recordsStore = useRecordsStore()

    const exerciseId = '53eaa6a3-471b-4fb4-98f5-234835510f9d'
    const date = '2022-06-29'

    const set = {
      index: 0,
      reps: 5,
      weight: 80,
      checkedAt: '123',
    }

    const actual = recordsStore.getSetRecordEmoji(exerciseId, date, set)

    expect(actual).toBe('')
  })

  it('should not return an emoji for an unchecked set', () => {
    const recordsStore = useRecordsStore()

    const exerciseId = '53eaa6a3-471b-4fb4-98f6-234835510f9d'
    const date = '2022-06-29'
    const recordData = {
      userId: date,
      exerciseRecords: [
        {
          exerciseId: exerciseId,
          repMaxes: [
            {
              index: 0,
              nbrReps: 5,
              weight: 80,
              date: '',
            },
          ],
        },
      ],
    }
    recordsStore.records = [recordData]

    const set = {
      index: 0,
      reps: 5,
      weight: 80,
    }

    const actual = recordsStore.getSetRecordEmoji(exerciseId, date, set)

    expect(actual).toBe('')
  })

  it('should not return an emoji for the same weight but another date', () => {
    const recordsStore = useRecordsStore()

    const exerciseId = '53eaa6a3-471b-4fb4-98f6-234835510f9d'
    const date = '2022-06-29'
    const recordData = {
      userId: '',
      exerciseRecords: [
        {
          exerciseId: exerciseId,
          repMaxes: [
            {
              index: 0,
              nbrReps: 5,
              weight: 80,
              date: '2022-05-01',
            },
          ],
        },
      ],
    }
    recordsStore.records = [recordData]

    const set = {
      index: 0,
      reps: 5,
      weight: 80,
      checkedAt: '123',
    }

    const actual = recordsStore.getSetRecordEmoji(exerciseId, date, set)

    expect(actual).toBe('')
  })
})
