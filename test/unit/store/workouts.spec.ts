import { setActivePinia, createPinia } from 'pinia'
import { beforeEach, afterEach, describe, expect, it, vi } from 'vitest'
import type { Workout } from '~/models/workout'
import { clone } from '~/utils/clone'
import defaultWorkout from '~/test/testdata/workout0.json'
import workout1 from '~/test/testdata/workout1.json'
import workout1Expected from '~/test/testdata/workout1_expected.json'
import { useWorkoutsStore } from '~/store/workouts'

describe('workoutsStore', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
  })

  afterEach(() => {
    vi.restoreAllMocks()
  })

  it('find() should be falsy if the workout was not found ', async () => {
    const workoutsStore = useWorkoutsStore()

    vi.stubGlobal('$fetch', vi.fn())
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const actual = await workoutsStore.find('doesnotexist')

    expect(actual).toBeFalsy()
  })

  it('save() should save a new workout', async () => {
    const workoutsStore = useWorkoutsStore()

    const workout: Workout = defaultWorkout

    vi.stubGlobal('$fetch', vi.fn().mockResolvedValue(workout))
    vi.stubGlobal('useRequestHeaders', vi.fn())

    await workoutsStore.save(workout)

    const result = await workoutsStore.find(workout.id!)

    expect(result).toEqual(workout)
  })

  it('save() should update an existing workout', async () => {
    const workoutsStore = useWorkoutsStore()

    const workout0: Workout = defaultWorkout

    vi.stubGlobal('$fetch', vi.fn().mockResolvedValue(workout0))
    vi.stubGlobal('useRequestHeaders', vi.fn())

    await workoutsStore.save(workout0)

    const workout1: Workout = clone(workout0)
    workout1.date = 'test'

    vi.stubGlobal('$fetch', vi.fn().mockResolvedValue(workout1))

    await workoutsStore.save(workout1)

    const result = await workoutsStore.find(workout1.id!)

    expect(result).toEqual(workout1)
  })

  it('DeleteExerciseCommand should update exercise indices afterwards', async () => {
    const workoutsStore = useWorkoutsStore()

    const workout: Workout = workout1

    vi.stubGlobal(
      '$fetch',
      vi.fn().mockImplementation((_, options) => {
        return Promise.resolve(JSON.parse(options.body))
      }),
    )
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const savedWorkout = await workoutsStore.save(workout)

    const command = {
      commandType: 'delete-exercise',
      workoutId: savedWorkout.id!,
      exercise: savedWorkout.data.exercises[1],
    }

    await workoutsStore.processCommand(command)

    const result = await workoutsStore.find(workout.id!)

    expect(result).toEqual(workout1Expected)
  })

  it('AddExerciseCommand should update exercise indices afterwards', async () => {
    const workoutsStore = useWorkoutsStore()

    const workout: Workout = workout1

    vi.stubGlobal(
      '$fetch',
      vi.fn().mockImplementation((_, options) => {
        return Promise.resolve(JSON.parse(options.body))
      }),
    )
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const savedWorkout = await workoutsStore.save(workout)

    const command = {
      commandType: 'add-exercise',
      workoutId: savedWorkout.id!,
      exerciseId: '53eaa6a3-471b-4fb4-98e5-bbbbbbbbbbbb',
    }

    await workoutsStore.processCommand(command)

    const result = await workoutsStore.find(workout.id!)

    const expected = {
      exerciseId: command.exerciseId,
      index: 3,
      sets: [
        {
          index: 0,
          reps: 0,
          weight: 0,
          durationSeconds: null,
          checkedAt: null,
        },
      ],
    }

    expect(result!.data.exercises[3]).toEqual(expected)
  })

  it('UpdateSetCommand should update a set', async () => {
    const workoutsStore = useWorkoutsStore()

    const workout: Workout = workout1

    vi.stubGlobal(
      '$fetch',
      vi.fn().mockImplementation((_, options) => {
        return Promise.resolve(JSON.parse(options.body))
      }),
    )
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const savedWorkout = await workoutsStore.save(workout)

    const command = {
      commandType: 'update-set',
      workoutId: savedWorkout.id!,
      exerciseIndex: 2,
      exerciseSet: {
        index: 0,
        reps: 6,
        weight: 38.5,
        checkedAt: 'some-date',
      },
    }

    await workoutsStore.processCommand(command)

    const result = await workoutsStore.find(workout.id!)

    expect(result!.data.exercises[2].sets[0]).toEqual(command.exerciseSet)
  })

  it('DeleteSetCommands should delete a set', async () => {
    const workoutsStore = useWorkoutsStore()

    const workout: Workout = workout1

    vi.stubGlobal(
      '$fetch',
      vi.fn().mockImplementation((_, options) => {
        return Promise.resolve(JSON.parse(options.body))
      }),
    )
    vi.stubGlobal('useRequestHeaders', vi.fn())

    const savedWorkout = await workoutsStore.save(workout)

    const command = {
      commandType: 'delete-set',
      workoutId: savedWorkout.id!,
      exerciseIndex: 1,
      setIndex: 0,
    }

    await workoutsStore.processCommand(command)

    const result = await workoutsStore.find(workout.id!)

    expect(result!.data.exercises[1].sets).toEqual([])
  })
})
