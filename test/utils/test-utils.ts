import { mount, flushPromises } from '@vue/test-utils'
import type { VueWrapper, MountingOptions } from '@vue/test-utils'
import { defineComponent } from 'vue'
import type { ComponentPublicInstance } from '@nuxtjs/composition-api'
import WButton from '@/components/WButton.vue'

// Workaround for [Vue warn]: Failed to resolve component:
// maybe there is a better way to do this?
const componentOptions = {
  global: {
    components: {
      WButton,
    },
  },
}

export async function mountWithSuspense<Component extends ComponentPublicInstance, Props>(
  component: new () => Component,
  options: MountingOptions<Props>,
): Promise<VueWrapper<ComponentPublicInstance>> {
  const wrapper = defineComponent({
    components: {
      [component.name]: component,
    },
    props: Object.keys(options.props ?? {}),
    template: `<suspense><${component.name} v-bind="$props" /></suspense>`,
  })

  const result = mount(wrapper, { ...componentOptions, ...options } as any)

  await flushPromises()

  return result
}
