export async function fetchWithRetry(url: string, opts: any, tries = 3) {
  let error = null

  for (let i = 0; i < tries; i++) {
    try {
      return await $fetch(url, opts)
    } catch (err: any) {
      if (err.response) {
        const response: Response = err.response
        if (response.status === 400 || response.status === 500) {
          error = {
            message: err.data + ' (code ' + response.status + ')',
          }
          throw error
        }
      } else {
        error = { message: err }
      }
    }
  }

  throw error
}
