import { defineConfig } from 'vitest/config'
import tsconfigPaths from 'vite-tsconfig-paths'
import vue from '@vitejs/plugin-vue'

export default defineConfig({
  test: {
    environment: 'jsdom',
    deps: {
      inline: [/@nuxt\/test-utils-edge/],
    },
    testTimeout: 5000,
    coverage: {
      exclude: [
        'playwright.config.ts',
        'tailwind.config.js',
        'vitest.config.ts',
        'nuxt.config.js',
        '.eslintrc.js',
        '.nuxt/**',
        'node_modules/**',
        'test/**',
      ],
    },
  },
  plugins: [tsconfigPaths(), vue()],
})
